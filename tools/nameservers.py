#!/usr/bin/env python
# encoding: utf-8

u"""
Поисковик китайских DNS-серверов

Usage:
    nameservers.py [--limit=<count>] [--pool=<size>] [--timeout=<timeout>] [--debug]
    nameservers.py (-h | --help)

Options:
    -h --help               Помощь
    --limit=<count>         Максимальное количество DNS-серверов [default: 10]
    --pool=<size>           Размер пула проверки [default: 30]
    --timeout=<timeout>     Максимальная время ожидания ответа от DNS-сервера [default: 3.0]
    --debug                 Отладочная информация
"""

from gevent.monkey import patch_all; patch_all()

from sys import exit
from pprint import pformat
from logging import getLogger, basicConfig, DEBUG
from logging.handlers import RotatingFileHandler
from re import match
from random import shuffle
from functools import partial

from docopt import docopt
from grab import Grab
from dns.resolver import Resolver
from dns.exception import DNSException
from gevent import spawn, Greenlet
from gevent.pool import Pool


logger = getLogger('nameservers')
logger.setLevel(DEBUG)
logger.addHandler(RotatingFileHandler('/tmp/nameservers.log', backupCount=8))


class AsyncWorker(object):
    def __init__(self, pool_size, **kwargs):
        self.pool_size = pool_size
        self.__dict__.update(kwargs)

    def tasks_generator(self):
        u"""Генератор кортежей для передачи в конструктор гринлетов"""
        yield

    def run(self):
        pool = Pool(self.pool_size)
        def wrapper():
            for task in self.tasks_generator():
                if not task:
                    continue
                pool.wait_available()
                greenlet = Greenlet(*task)
                greenlet.link_value(partial(lambda task, greenlet: self.task_finished(task, greenlet.value),
                                            task))
                greenlet.link_exception(partial(lambda task, greenlet: self.task_failed(task, greenlet.exception),
                                                task))
                pool.start(greenlet)
            pool.join()
        spawn(wrapper).join()

    def task_finished(self, *args):
        u"""Вызовется при успешной отработке гринлета"""

    def task_failed(self, *args):
        u"""Вызовется при ошибке"""


class Nameserver(object):
    __slots__ = ('location', 'address', )

    def __init__(self, address, location=''):
        self.location = location.strip()
        self.address = address.strip()

    def _resolve(self, domain, timeout=None):
        resolver = Resolver()
        resolver.reset()
        resolver.cache = False
        resolver.nameservers = [self.address, ]
        if timeout:
            resolver.lifetime = timeout
        try:
            answer = resolver.query(domain)
            return [
                item.address
                for item in answer
            ]
        except DNSException:
            pass
        return ()

    def is_valid(self, timeout=None):
        return self.resolve('google.com', timeout=timeout)

    def resolve(self, domain, timeout=None):
        return self._resolve(domain, timeout=timeout)

    def __unicode__(self):
        return u'NS[%s/%s]' % (self.location.encode('unicode_escape'),
                               self.address.encode('unicode_escape'))

    __repr__ = __unicode__


class Nameservers(object):
    def __init__(self, check_timeout=3.0, check_pool_size=10):
        self._nameservers = None
        self._check_timeout = check_timeout
        self._check_pool_size = check_pool_size

    def __iter__(self):
        u"""Рабочие сервера"""
        if not self._nameservers:
            self._nameservers = self._check(self._find(),
                                            self._check_timeout)
        for nameserver in self._nameservers:
            yield nameserver

    def choose(self, count):
        u"""Выборка по одному рабочему серверу в count городах"""
        grouped = {}
        for item in self:
            if item.location not in grouped:
                grouped[item.location] = []
            grouped[item.location].append(item)
        groups = grouped.values()
        shuffle(groups)
        return [nameservers[0] for nameservers in groups[:count]]

    def _find(self):
        logger.debug(u'Получение общего списка DNS-серверов...')

        grab = Grab()
        grab.go('http://public-dns.tk/nameserver/cn.html')
        found = []
        for item in grab.xpath_list('//*[@class="nameservers"]/tbody/tr'):
            found.append(Nameserver(item.xpath('./td[1]/text()')[0],
                                     item.xpath('./td[3]/text()')[0]))
        found = filter(
            lambda item: item.location and
                         match(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', item.address),
            found
        )
        logger.debug(u'Всего найдено {} DNS-серверов'.format(len(found)))
        logger.debug(u'Найденые DNS-сервера:\n{}'.format(pformat(found)))

        return found

    def _check(self, found, check_timeout=3.0):
        logger.debug(u'Фильтрация рабочих DNS-серверов')

        valid = []

        class Checker(AsyncWorker):
            def tasks_generator(self):
                for nameserver in found:
                    yield (Nameserver.is_valid, nameserver, check_timeout)

            def task_finished(self, task, result, *args):
                if not result:
                    return
                _, nameserver, _ = task
                valid.append(nameserver)

        checker = Checker(self._check_pool_size)
        checker.run()

        logger.debug(u'Рабочих {} DNS-серверов'.format(len(valid)))
        logger.debug(u'Рабочие DNS-сервера:\n{}'.format(pformat(valid)))

        return valid


if __name__ == '__main__':
    arguments = docopt(__doc__)

    if arguments.get('--debug'):
        basicConfig(level=DEBUG)

    timeout = float(arguments.get('--timeout'))
    pool = int(arguments.get('--pool'))
    limit = int(arguments.get('--limit'))

    nameservers = Nameservers(timeout, pool).choose(limit)
    if not nameservers:
        logger.error(u'Не найдено DNS-серверов.')
        exit(1)

    for nameserver in nameservers:
        print nameserver.address
