#!/usr/bin/env python
# encoding: utf-8

u"""
Сборщик доменов с en.greatfire.org

Usage:
    domains.py [--threads=<count>] [--limit=<count>] [--cache] [--debug]
    domains.py (-h | --help)

Options:
    -h --help           Помощь
    --limit=<count>     Включение ограничения на количество страниц
    --cache             Кешировать результаты запроса страниц
    --threads=<count>   Количество потоков парсинга [default: 1]
    --debug             Отладочная информация
"""


from re import search, compile
from logging import getLogger, basicConfig, DEBUG
from logging.handlers import RotatingFileHandler

from docopt import docopt
from pybloom import BloomFilter
from grab.spider import Spider, Task


logger = getLogger('domains-finder')
logger.setLevel(DEBUG)
logger.addHandler(RotatingFileHandler('/tmp/domains.log', backupCount=8))


regex_domain = compile(r'(https?://)?((www.)?([^?:/]+)).*')
regex_ip = compile('\d{,3}\.\d{,3}\.\d{,3}\.\d{,3}')

get_domain = lambda url: regex_domain.search(url).group(2).lower()


class DomainsFinder(Spider):
    def __init__(self, tasks_limit=None, *args, **kwargs):
        super(DomainsFinder, self).__init__(*args, **kwargs)
        self.tasks_limit = tasks_limit
        self.bloom = BloomFilter(capacity=int(1e6), error_rate=0.001)

    def task_generator(self):
        logger.info(u'Поиск индексной страницы')
        grab = self.create_grab_instance()
        grab.go('https://en.greatfire.org/search/blocked')
        last_url = grab.xpath_list('//*[@class="pager-last last"]/a/@href')[0]
        last_page_index = int(search(r'page=(\d+)', last_url).group(1))
        logger.info(u'Найдено %d страниц для обхода' % (last_page_index + 1))
        for page_index in xrange(0, (self.tasks_limit or last_page_index) + 1):
            yield Task(
                name='page',
                url='https://en.greatfire.org/search/blocked/?page=%d' % page_index
            )

    def task_page(self, grab, task):
        domains = grab.xpath_list('//*[@id="block-system-main"]//table/tbody/tr/td[1]/a/text()')

        count = 0
        for domain in map(get_domain, domains):
            if regex_ip.match(domain):
                continue
            if domain in self.bloom:
                continue
            self.bloom.add(domain)
            print u'{}'.format(domain)
            count += 1

        logger.info(u'Найдено {} урлов, среди них {} новых и корректных доменов на странице {}'.format(
            len(domains),
            count,
            grab.response.url
        ))

    def shutdown(self):
        logger.info(u'Завершен поиск доменов')


if __name__ == '__main__':
    arguments = docopt(__doc__)

    if arguments.get('--debug'):
        basicConfig(level=DEBUG)

    tasks_limit = arguments.get('--limit')
    if tasks_limit:
        tasks_limit = int(tasks_limit)

    finder = DomainsFinder(thread_number=int(arguments.get('--threads')),
                           tasks_limit=tasks_limit)
    if arguments.get('--cache'):
        finder.setup_cache(database='domains-finder-cache')
    finder.run()
