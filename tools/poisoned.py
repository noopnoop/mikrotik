#!/usr/bin/env python
# encoding: utf-8

u"""
Фильтрация доменов для которых отравлен кэш

Usage:
    poisoned.py [--limit=<count>] [--pool=<size>] [--timeout=<timeout>] [--debug] [--dns=<nameserver>] [--ignore=<list>]
    poisoned.py (-h | --help)

Options:
    -h --help               Помощь
    --limit=<count>         Максимальное количество DNS-серверов [default: 10]
    --pool=<size>           Размер пула проверки [default: 30]
    --timeout=<timeout>     Максимальная время ожидания ответа от DNS-сервера [default: 3.0]
    --dns=<nameserver>      DNS-сервер по-умолчанию [default: 8.8.8.8]
    --ignore=<list>         Первые октеты для игнора [default: 74.125,173.194]
    --debug                 Отладочная информация
"""

from gevent.monkey import patch_all; patch_all()

from sys import stdin
from logging import getLogger, basicConfig, DEBUG
from logging.handlers import RotatingFileHandler

from docopt import docopt
from gevent import spawn, joinall

from nameservers import AsyncWorker, Nameservers, Nameserver


logger = getLogger('poisoned')
logger.setLevel(DEBUG)
logger.addHandler(RotatingFileHandler('/tmp/poisoned.log', backupCount=8))


class Domain(object):
    __slots__ = ('name', )

    def __init__(self, name):
        self.name = name.strip()

    def __unicode__(self):
        return u'DOMAIN[%s]' % self.name

    __repr__ = __unicode__

    def check(self, nameservers, check_timeout=3.0):
        u"""Возвращает резолв домена от кажного DNS-сервера из списка"""
        tasks = [
            spawn(nameserver.resolve, self.name, check_timeout)
            for nameserver in nameservers
        ]
        joinall(tasks)
        return [task.value for task in tasks]


def compare_addresses(base, other, ignore=None):
    two_octets = lambda item: '.'.join(item.split('.')[:2])
    base_copy = {
        _: two_octets(_)
        for _ in base
    }
    other_copy = {
        _: two_octets(_)
        for _ in other
    }
    diff = set(base_copy.values()).difference(set(other_copy.values()))
    result = []
    if diff:
        result = [
            address
            for (address, octets) in base_copy.iteritems()
            if octets in diff and (True if not ignore else octets not in ignore)
        ]
    return result


if __name__ == '__main__':
    arguments = docopt(__doc__)

    if arguments.get('--debug'):
        basicConfig(level=DEBUG)

    timeout = float(arguments.get('--timeout'))
    pool = int(arguments.get('--pool'))
    limit = int(arguments.get('--limit'))
    default_dns = arguments.get('--dns')
    ignore_list = arguments.get('--ignore').split(',')

    class DomainsChecker(AsyncWorker):
        def tasks_generator(self):
            nameservers = Nameservers(timeout, pool).choose(limit)
            nameservers.insert(0, Nameserver(default_dns))

            for line in stdin:
                yield (Domain.check, Domain(line), nameservers, timeout)

        def task_finished(self, task, addresses, *args):
            _, domain, _, _ = task

            base = addresses[0]
            others = set(
                address
                for group in addresses[1:]
                for address in group
            )
            diff = compare_addresses(others, base, ignore_list)

            logger.debug(u'Проверка домена "{}":\n'
                         u'\tбазовые адреса: {}\n'
                         u'\tостальные: {}\n'
                         u'\tразница с учетом игнорируемых: {}'.format(
                domain,
                base,
                others,
                diff
            ))

            if not diff:
                return

            for address in base:
                print u'{},{}'.format(domain.name, address)

    DomainsChecker(pool).run()
