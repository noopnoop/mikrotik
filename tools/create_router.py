#!/usr/bin/env python
# encoding: utf-8

u"""
Добавление роутера

Usage:
    create_router.py <router_id> <serial_number> -u=<username> [-p=<password>]
    create_router.py (-h | --help)

Options:
    -h --help           Помощь
    <router_id>         ID роутера
    <serial_number>     Серийный номер
    -u=<username>       Имя пользователя
    -p=<password>       Пароль
"""

from time import sleep
from getpass import getpass
from logging import getLogger, INFO, StreamHandler

from requests import Session
from docopt import docopt


logger = getLogger('create-router')
logger.addHandler(StreamHandler())
logger.setLevel(INFO)


BASE_URL = 'http://23.88.165.139:2080'
# BASE_URL = 'http://localhost:8000'


fields = {
    'ssl_certificate': 'Certificate',
    'ssl_key': 'Key',
    'ssl_ca': 'CA',
}


def show_errors(errors):
    print u'Errors:'
    for key, items in errors.items():
        if isinstance(items, (list, tuple)):
            print u'\t{}'.format(key)
            for item in items:
                print u'\t\t{}'.format(item)

        else:
            print u'\t{}: {}'.format(key, items)


if __name__ == '__main__':
    arguments = docopt(__doc__)
    if not arguments.get('-p'):
        arguments['-p'] = getpass()

    session = Session()
    session.auth = (arguments.get('-u'), arguments.get('-p'))

    response = session.post('{}/api/routers/'.format(BASE_URL), data={
        'router_id': arguments.get('<router_id>'),
        'serial_number': arguments.get('<serial_number>'),
    })
    if response.status_code != 201:
        logger.error(u'Ошибка при создании роутера')
        show_errors(response.json())
        exit(1)

    router = response.json()
    logger.info(u'Роутер успешно создан. Ожидание сертификатов...')

    data = None
    for _ in xrange(30):
        sleep(5.0)

        response = session.get('{}/api/keys/{}/'.format(BASE_URL, router.get('router_id')))
        if response.status_code != 200:
            logger.error(u'Ошибка при запросе сертификатов')
            show_errors(response.json())
            exit(1)

        data = response.json()
        if any(data.values()):
            break
        else:
            data = None

    if any(data.values()):
        logger.info(u'Сертификаты готовы')

        for key, value in data.items():
            print fields.get(key, key).capitalize()
            print value
            print
            print

    else:
        logger.error(u'Время ожидания истекло: сертификаты не готовы')
