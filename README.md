Установка
=========

* `git clone https://alexey_grom@bitbucket.org/alexey_grom/mikrotik.git`
* `cd mikrotik`
* `virtualenv venv`
* `source venv/bin/activate`
* `cd src`
* создать БД и прописать досупы в `project/local_settings.py`
* `fab local_update`
* `./manage.py createsuperuser` # создание пользователя, если еще нет


Обновление изменение
====================

* `cd mikrotik`
* `git pull`
* `source venv/bin/activate`
* `cd src`
* `fab local_update`


Запуск
======

* `./manage.py celery worker -l info -c 30 -P eventlet` # воркер celery для выполнения асинхронных задач
* `./manage.py runserver`
* `http://localhost:8000/admin/`