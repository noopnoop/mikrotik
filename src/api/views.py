# encoding: utf-8

from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin

from backend.models import Router


class OwnRoutersMixin(object):
    def get_queryset(self):
        queryset = super(OwnRoutersMixin, self).get_queryset()
        if not self.request.user.is_superuser:
            queryset = queryset.filter(added_by=self.request.user)
        return queryset


class RouterViewSet(OwnRoutersMixin, CreateModelMixin, RetrieveModelMixin, GenericViewSet):
    class RouterSerializer(ModelSerializer):
        class Meta:
            model = Router
            fields = ('router_id', 'serial_number', )

    model = Router
    serializer_class = RouterSerializer


class KeysViewSet(OwnRoutersMixin, RetrieveModelMixin, GenericViewSet):
    class KeysSerializer(ModelSerializer):
        class Meta:
            model = Router
            fields = ('ssl_certificate', 'ssl_key', 'ssl_ca', )

    model = Router
    serializer_class = KeysSerializer
