# encoding: utf-8

from django.conf.urls import patterns, url
from rest_framework.routers import DefaultRouter

from views import RouterViewSet, KeysViewSet


router = DefaultRouter()
router.register(r'routers', RouterViewSet)
router.register(r'keys', KeysViewSet)

urlpatterns = patterns('', ) + router.urls
