# encoding: utf-8

from re import match

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper, Layout
from crispy_forms.layout import Row, Div, Field, Submit


def ssid_validator(value):
    if match(r'[\d\w \.\-\(\)]', value):
        return
    raise ValidationError(_(u'SSID may contain: a-z0-9.-()'))


class UpdateSettingsForm(forms.Form):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.attrs = {
        'data_parsley_validate': '',
    }
    helper.form_class = 'js-settings-form'
    helper.layout = Layout(
        Row(Div(Field('wifi_enabled'), css_class='col-xs-12')),
        Row(Div(Field('wifi_ssid'), css_class='col-xs-12')),
        Row(Div(Field('wifi_password'), css_class='col-xs-12')),
        Row(Div(Field('wifi_channel'), css_class='col-xs-12')),
        Row(Div(Submit('submit', _(u'Apply'), css_class='btn-success'), css_class='col-xs-12')),
    )

    wifi_enabled = forms.BooleanField(required=False,
                                      label=_(u'WiFi enabled'))

    wifi_ssid = forms.CharField(max_length=255,
                                required=False,
                                validators=[ssid_validator, ],
                                label=_(u'WiFi SSID'))
    wifi_password = forms.CharField(max_length=255,
                                    required=False,
                                    label=_(u'WiFi password'))
    wifi_channel = forms.ChoiceField(required=False,
                                     choices=(('2412', '2412'),
                                              ('2417', '2417'),
                                              ('2422', '2422'),
                                              ('2427', '2427'),
                                              ('2432', '2432'),
                                              ('2437', '2437'),
                                              ('2442', '2442'),
                                              ('2447', '2447'),
                                              ('2452', '2452'),
                                              ('2457', '2457'),
                                              ('2462', '2462'),
                                              ('auto', 'auto'), ),
                                     label=_(u'WiFi channel'))

    def clean_wifi_enabled(self):
        value = self.cleaned_data.get('wifi_enabled')
        if value:
            self.fields.get('wifi_ssid').required = True
            self.fields.get('wifi_password').required = True
            self.fields.get('wifi_channel').required = True
        return value
