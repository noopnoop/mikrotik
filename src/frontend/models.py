# encoding: utf-8

from django.db import models
from django.utils.translation import ugettext_lazy as _


class FAQItem(models.Model):
    question = models.TextField(verbose_name=_(u'Вопрос'))
    answer = models.TextField(verbose_name=_(u'Ответ'))

    def __unicode__(self):
        return self.question

    class Meta:
        verbose_name = _(u'Часто задаваемый вопрос')
        verbose_name_plural = _(u'Часто задаваемые вопросы')
