# encoding: utf-8

from weakref import ref
from urllib import unquote
from json import dumps

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.conf.urls import patterns, include, url
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.decorators import method_decorator
from celery import current_app
from celery.result import AsyncResult

from backend.auth import logged_in_or_basicauth


class LayoutMixin(object):
    u"""
    Догружает контекст параметрами
    """

    title = None
    is_show_breadcrumbs = True

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, request, *args, **kwargs):
        return super(LayoutMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(LayoutMixin, self).get_context_data(**kwargs)
        if self.title:
            context_data['title'] = self.title
            context_data['is_show_breadcrumbs'] = self.is_show_breadcrumbs
        return context_data


class ClientRequiredMixin(object):
    u"""
    Проверка что пользователь пришел с роутера и догружает контекст данными о нем
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.client.is_client:
            raise PermissionDenied()
        return super(ClientRequiredMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(ClientRequiredMixin, self).get_context_data(**kwargs)
        context_data['client'] = ref(self.request.client)
        return context_data


class BasicUserAuthMixin(object):
    permissions = ()

    def user_auth_func(self, request, username, password):
        def check_permissions(user):
            for permission in self.permissions:
                if not user.has_perm(permission):
                    return False
            return True

        if request.user.is_authenticated():
            if request.user.is_superuser:
                return True
            return check_permissions(request.user)

        user = get_user_model().objects.\
            filter(username=username).\
            first()
        if not user:
            return False
        if not user.check_password(password):
            return False
        if not user.is_superuser and not check_permissions(request.user):
            return False

        request.user = user
        return True

    def dispatch(self, request, *args, **kwargs):
        @logged_in_or_basicauth(self.user_auth_func)
        def wrapper(request, *args, **kwargs):
            return super(BasicUserAuthMixin, self).dispatch(request, *args, **kwargs)
        return wrapper(request, *args, **kwargs)


class TaskBundleMeta(type):
    u"""
    Метакласс для проверки правильности описания класса
    """

    def __new__(cls, what, bases, attrs):
        u"""Создание класса"""

        definition = super(TaskBundleMeta, cls).__new__(cls, what, bases, attrs)

        # сборка мета-данных из описания класса
        definition._meta = {}
        # for base in bases:
        #     definition._meta.update(getattr(base, '_meta', {}))
        meta = attrs.get('Meta', None)
        if meta:
            definition._meta.update(**dict(filter(lambda item: not item[0].startswith('_'),
                                                  meta.__dict__.items())))

        # проверка что имя задачи задано
        if not definition._meta.get('task_name', False) and not definition._meta.get('abstract', False):
            raise AttributeError('`task_name` attribute is empty for class `%s`!' % what)

        return definition

    def __call__(cls, *args, **kwargs):
        u"""Инстанцирование экземпляра класса"""
        # проверка что класс не абстрактный
        if cls._meta.get('abstract', False):
            raise NotImplementedError('Instancing abstract class')
        return super(TaskBundleMeta, cls).__call__(*args, **kwargs)


class TaskBundle(object):
    u"""
    Набор вьюх для работы с задачей celery
    """

    # TODO: механизм кеширования результатов

    __metaclass__ = TaskBundleMeta

    @classmethod
    def urls(cls):
        u"""Сборка url'ов"""

        url_patterns = patterns('', )
        prefix = cls.__name__.lower()

        def wrapper(method):
            def dispatch(*args, **kwargs):
                instance = cls()
                return method(instance, *args, **kwargs)
            return dispatch

        url_patterns += [
            url(r'^$', wrapper(cls.create_view), name='%s_create' % prefix),
            url(r'^(?P<task_id>[^/]+)/state$', wrapper(cls.state_view), name='%s_state' % prefix),
            url(r'^(?P<task_id>[^/]+)/revoke$', wrapper(cls.revoke_view), name='%s_revoke' % prefix),
        ]

        return include(url_patterns)

    @classmethod
    def spec(cls):
        u"""Url'ы для передачи в js"""
        prefix = cls.__name__.lower()
        return {
            'create': unquote(reverse('%s_create' % prefix)),
            'state': unquote(reverse('%s_state' % prefix, kwargs={'task_id': '<%= task_id %>'})),
            'revoke': unquote(reverse('%s_revoke' % prefix, kwargs={'task_id': '<%= task_id %>'})),
        }

    def create_view(self, request):
        u"""Запуск задачи"""

        args, kwargs = (), {}

        # проверка прав
        if not self.is_can_create(request):
            raise PermissionDenied()

        # валидация формы если требуется
        form_class = self._meta.get('form_class', None)
        if form_class:
            form = form_class(data=request.REQUEST, files=request.FILES)
            if not form.is_valid():
                return self.response(errors=form.errors)
            kwargs.update({
                'form': form.cleaned_data,
            })

        # дополнение пользовательским контекстом
        kwargs.update(self.get_task_context(request) or {})

        # запуск задачи
        asyncresult = current_app.send_task(
            self._meta.get('task_name'),
            args,
            kwargs
        )
        return self.response(data=self.task_data(asyncresult))

    def state_view(self, request, task_id):
        u"""Состояние выполнения задачи"""
        asyncresult = AsyncResult(task_id)
        if not self.is_can_control(request, asyncresult):
            raise PermissionDenied()
        return self.response(data=self.task_data(asyncresult))

    def revoke_view(self, request, task_id):
        u"""Отмена выполнения задачи"""
        asyncresult = AsyncResult(task_id)
        if not self.is_can_control(request, asyncresult):
            raise PermissionDenied()
        asyncresult.revoke(terminate=True)
        return self.response(data={})

    def task_data(self, asyncresult):
        u"""JSON состояния задачи"""

        result = {
            'task_id': asyncresult.id,

            'ready': asyncresult.ready(),  # задача завершена в принципе
            'successful': asyncresult.successful(),  # успешно
            'failed': asyncresult.failed(),  # упало

            'state': asyncresult.state,
            # 'status': asyncresult.status,
            'result': None,
        }

        if asyncresult.successful():
            result.update({
                'result': asyncresult.result,
            })

        return result

    def response(self, data=None, errors=None):
        u"""Шорткат для формирования ответа"""

        if data is None and errors is None:
            raise ValueError()
        result = {
            'result': not errors,
        }
        if data:
            result.update({
                'data': data,
            })
        if errors:
            result.update({
                'errors': errors,
            })
        return HttpResponse(dumps(result))

    def get_task_context(self, request):
        u"""Расширение контекста для передачи в задачу"""
        return None

    def is_can_create(self, request):
        u"""Проверка что задача принадлежит пользователю"""
        return True

    def is_can_control(self, request, asyncresult):
        u"""Проверка что задача принадлежит пользователю"""
        return True

    @property
    def task_name(self):
        return self._meta.get('task_name')

    class Meta:
        abstract = True
        task_name = None
        form_class = None
