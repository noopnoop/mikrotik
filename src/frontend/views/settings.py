# encoding: utf-8

from .base import TaskBundle
from ..forms import UpdateSettingsForm


class FrontendUserMixin(object):
    def get_task_context(self, request):
        return {
            'client': getattr(request, 'client', None),
        }

    def is_can_create(self, request):
        client = getattr(request, 'client', None)
        return client is not None and client.router is not None

    def is_can_control(self, request, asyncresult):
        client = getattr(request, 'client', None)
        # TODO: класть task-id в сессию и проверять
        return client is not None


class ReceiveSettingsBundle(FrontendUserMixin, TaskBundle):
    # FIXIT: не перезапускать задачу, если уже выполняется

    class Meta:
        task_name = 'get_settings'


class UpdateSettingsBundle(FrontendUserMixin, TaskBundle):
    class Meta:
        task_name = 'put_settings'
        form_class = UpdateSettingsForm
