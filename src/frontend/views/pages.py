# encoding: utf-8

from django.views.generic import TemplateView, ListView
from django.utils.translation import ugettext as _

from ..forms import UpdateSettingsForm
from .base import ClientRequiredMixin, LayoutMixin
from .settings import ReceiveSettingsBundle, UpdateSettingsBundle

from ..models import FAQItem


class PrivatePage(LayoutMixin, ClientRequiredMixin, TemplateView):
    pass


class PromoView(LayoutMixin, TemplateView):
    template_name = 'frontend/promo.html'
    title = _(u'Main')
    is_show_breadcrumbs = False


class HelpView(LayoutMixin, TemplateView):
    template_name = 'frontend/help.html'
    title = _(u'Help')


class FAQView(LayoutMixin, ListView):
    template_name = 'frontend/faq.html'
    title = _(u'FAQ')
    model = FAQItem


class StatusView(PrivatePage):
    template_name = 'frontend/status.html'
    title = _(u'Status')


class SettingsView(PrivatePage):
    template_name = 'frontend/settings.html'
    title = _(u'Settings')

    def get_context_data(self, **kwargs):
        context_data = super(SettingsView, self).get_context_data(**kwargs)
        context_data.update({
            'form': UpdateSettingsForm(),
            'layout': {
                'urls': {
                    'settings': {
                        'receive': ReceiveSettingsBundle.spec(),
                        'update': UpdateSettingsBundle.spec(),
                    },
                }
            }
        })
        return context_data
