from django.core.urlresolvers import reverse
from django.http.response import HttpResponsePermanentRedirect
from django.utils import translation
from ipware.ip import get_ip

from backend.models import Router


__all__ = ['FrontendMiddleware', ]


class ClientInfo(object):
    def __init__(self, request):
        self.ip = get_ip(request)
        self.router = Router.objects.filter(ip=self.ip).first()
        if not self.router and request.user.is_superuser:  # TODO: -debug
            #
            router_id = request.GET.get('router_id', None) or \
                        request.session.get('previous_router_id', None)
            #
            if router_id:
                request.session['previous_router_id'] = router_id
                self.router = Router.objects.\
                    filter(router_id=router_id).\
                    first()

    @property
    def is_client(self):
        return self.router is not None

    def __eq__(self, other):
        return isinstance(other, ClientInfo) and self.ip == other.ip


class FrontendMiddleware(object):
    def process_request(self, request):
        request.client = ClientInfo(request)


class HomeRedirectMiddleware(object):
    def process_request(self, request):
        host = request.get_host()
        host = host.split(':')[0]

        path = request.get_full_path().rstrip('/')

        if path:
            return

        if host == 'settings':
            return HttpResponsePermanentRedirect(reverse('frontend:settings'))
        elif host == 'help':
            return HttpResponsePermanentRedirect(reverse('frontend:help'))
        return HttpResponsePermanentRedirect(reverse('frontend:promo'))


class AdminLocaleURLMiddleware(object):
    def process_request(self, request):
        if request.path.startswith('/guru/') or request.path.startswith('/rosetta/'):
            translation.activate('ru')
            request.LANGUAGE_CODE = 'ru'
