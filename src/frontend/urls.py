from django.conf.urls import patterns, include, url

from views import (PromoView, SettingsView, StatusView, HelpView, FAQView,
                   ReceiveSettingsBundle, UpdateSettingsBundle)


urlpatterns = patterns('',
    url(r'^', include(patterns('',
        url(r'^about/$', PromoView.as_view(), name='promo'),
        url(r'^settings/$', SettingsView.as_view(), name='settings'),
        url(r'^help/$', HelpView.as_view(), name='help'),
        url(r'^status/$', StatusView.as_view(), name='status'),
        url(r'^faq/$', FAQView.as_view(), name='faq'),
    ), namespace='frontend')),

    url(r'^settings/', include(patterns('',
        url(r'^receive/', ReceiveSettingsBundle.urls()),
        url(r'^update/', UpdateSettingsBundle.urls()),
    ))),

)
