# encoding: utf-8

from django import template
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _


register = template.Library()


@register.simple_tag(takes_context=True)
def main_menu_link(context, title, viewname, *args, **kwargs):
    request = context['request']
    url = reverse(viewname, args=args, kwargs=kwargs)
    return '<li class="%s"><a href="%s">%s</a></li>' % (
        'active' if request.get_full_path().split('?')[0] == url else '',
        url,
        _(title)
    )
