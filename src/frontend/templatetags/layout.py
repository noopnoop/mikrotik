# encoding: utf-8

from json import dumps

from django import template


register = template.Library()


@register.simple_tag(takes_context=True)
def layout_data(context):
    data = context.get('layout', {})
    return '<script type="application/json" ' \
           'class="js-layout-data">%s</script>' % dumps(data)
