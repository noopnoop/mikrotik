# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FAQItem'
        db.create_table(u'frontend_faqitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.TextField')()),
            ('question_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('question_zh_cn', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('answer', self.gf('django.db.models.fields.TextField')()),
            ('answer_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('answer_zh_cn', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'frontend', ['FAQItem'])


    def backwards(self, orm):
        # Deleting model 'FAQItem'
        db.delete_table(u'frontend_faqitem')


    models = {
        u'frontend.faqitem': {
            'Meta': {'object_name': 'FAQItem'},
            'answer': ('django.db.models.fields.TextField', [], {}),
            'answer_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'answer_zh_cn': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.TextField', [], {}),
            'question_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'question_zh_cn': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['frontend']