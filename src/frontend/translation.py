from modeltranslation.translator import translator, TranslationOptions

from .models import FAQItem


class FAQItemTranslationOptions(TranslationOptions):
    fields = ('question', 'answer', )
    fallback_values = {
        'question': '1',
        'answer': '2',
    }


translator.register(FAQItem, FAQItemTranslationOptions)
