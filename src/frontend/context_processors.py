# encoding: utf-8


def client(request):
    return {
        'is_authorized': request.client.is_client,
    }
