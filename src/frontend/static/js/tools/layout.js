(function () {
    var root = this;

    root.getLayoutData = _.memoize(function () {
        return JSON.parse($('.js-layout-data').text());
    });

})();
