(function () {
    var root = this;

    root.showPreloader = function () {
        $('body').prepend($(
            '<div id="facebookG-layout"></div>' +
            '<div id="facebookG">' +
                '<div id="blockG_1" class="facebook_blockG"></div>' +
                '<div id="blockG_2" class="facebook_blockG"></div>' +
                '<div id="blockG_3" class="facebook_blockG"></div>' +
            '</div>'));
    };

    root.hidePreloader = function () {
        var $els = $('body #facebookG, body #facebookG-layout');
        $els.fadeOut(500, function () {
            $els.remove();
        });
    };

})();
