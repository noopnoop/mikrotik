(function () {
    var root = this;

    /**
     * Враппер для управления задачей в бекэнде
     * @param spec спецификация интерфейса управления задачей `TaskBundle.spec`
     * @returns объект с методами create и revoke
     * @constructor
     */
    root.TaskWrapper = function (spec) {
        var deferred = $.Deferred();
        var taskId = null;

        var stateTimer = null;

        var checkState = function () {
            $.ajax({
                url: _.template(spec.state, {
                    task_id: taskId
                }),
                dataType: 'JSON',
                success: function (response) {
                    if (response.data.ready) {
                        if (response.data.successful) {
                            deferred.resolve(response.data.result);
                        } else {
                            deferred.reject({
                                'type': 'failed',
                                'errors': response.data.result
                            });
                        }
                    } else {
                        setTimeout(checkState, 1000);
                    }
                },
                error: function (response) {
                    deferred.reject({
                        type: 'transport'
                    });
                }
            });
        };

        var callCreate = function (options) {
            if (_.isFunction(options)) {
                options = {
                    beforeStart: options
                };
            }
            (options.beforeStart || function () {})();
            $.ajax({
                url: spec.create,
                method: 'POST',
                dataType: 'JSON',
                data: options.data || {},
                success: function (response) {
                    if (response.result) {
                        taskId = response.data.task_id;
                        stateTimer = setTimeout(checkState, 1000);
                    } else {
                        deferred.reject({
                            type: 'startup',
                            errors: response.errors
                        });
                    }
                },
                error: function (response) {
                    clearTimeout(stateTimer);
                    deferred.reject({
                        type: 'transport'
                    });
                }
            });
            return deferred;
        };

        var callRevoke = function () {
            clearTimeout(stateTimer);
            deferred.reject({
                'type': 'revoked'
            });
            $.ajax({
                url: _.template(spec.revoke, {
                    task_id: taskId
                })
            });
        };

        return {
            // Запуск
            create: callCreate,
            // Отмена
            revoke: callRevoke
        };
    };

})();
