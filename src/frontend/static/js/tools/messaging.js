(function () {
    var root = this;

    var showMessage = root.showMessage = function (options) {
        var defaults = {};
        $.growl(_.extend(defaults, options), {
            type: 'success'
        });
    };

    var showError = root.showError = function (options) {
        var defaults = {};
        $.growl(_.extend(defaults, options), {
            type: 'danger'
        });
    };

})();
