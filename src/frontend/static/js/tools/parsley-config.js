(function () {
    var root = this;

    /* Parsley.JS + Bootstrap 3 */
    _.extend(root.ParsleyConfig, {
        successClass: 'has-success',
        errorClass: 'has-error',
        classHandler: function($el){
            return $el.$element.closest('.form-group');
        },
        errorsWrapper: '<span></span>',
        errorTemplate: '<span class="help-block"></span>'
    });

})();
