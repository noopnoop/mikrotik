$(document).ready(function () {
    new (Backbone.View.extend({
        el: $('form'),

        events: {
            'submit': 'cleanErrors',
            'form:setFieldErrors': 'setFieldErrors'
        },

        initialize: function () {
            _.bindAll(this,
                'cleanErrors',
                'setFieldErrors');
        },

        cleanErrors: function () {
            var self = this;
            _.each(self.$el.find('input[type=text], select, textarea'), function (field) {
                ParsleyUI.removeError($(field).parsley(), 'serverValidation');
            });
        },

        setFieldErrors: function (event, fieldName, errors) {
            var self = this;
            var parsleyField = self.$el.find('[name="' + fieldName + '"]').parsley();
            _.each(errors, function (error) {
                ParsleyUI.addError(
                    parsleyField,
                    'serverValidation',
                    error
                );
            });
        }

    }))();
});
