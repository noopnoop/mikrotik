$(document).ready(function () {
    var SettingsModel = Backbone.Epoxy.Model.extend({
        defaults: {
            wifi_enabled: false,
            wifi_ssid: '',
            wifi_password: '',
            wifi_channel: ''
        },
        initialize: function () {
            SettingsModel.__super__.initialize.apply(this, arguments);
            _.bindAll(this, 'flush', 'onChanged');
            this.bind('change', this.onChanged);
            this.flush();
        },
        onChanged: function () {
            var prev = this._changed;
            this._changed = !_.isEqual(this._original, this.toJSON());
            if (prev != this._changed) {
                this.trigger('changed', this._changed);
            }
        },
        flush: function () {
            this._original = this.toJSON();
            this._changed = false;
            this.trigger('changed', false);
        }
    });

    window.view = new (Backbone.Epoxy.View.extend({
        el: $('.js-page-settings'),

        model: new SettingsModel(),

        events: {
            'submit .js-settings-form': 'saveSettings'
        },

        bindings: {
            '[name="wifi_enabled"]':    'checked:wifi_enabled',
            '[name="wifi_ssid"]':       'enabled:wifi_enabled,value:wifi_ssid',
            '[name="wifi_password"]':   'enabled:wifi_enabled,value:wifi_password',
            '[name="wifi_channel"]':    'enabled:wifi_enabled,value:wifi_channel'
        },

        initialize: function () {
            var self = this;
            if (!self.$el.length) {
                return;
            }
            _.bindAll(this,
                'getSettings', 'saveSettings',
                'processErrors', 'modelChanged');
            self.$form = self.$el.find('form');
            self.getSettings();
            self.model.bind('changed', self.modelChanged);
        },

        modelChanged: function (isChanged) {
            this.$el.find('[name="submit"]').prop('disabled', !isChanged);
        },

        processErrors: function (data) {
            var self = this;
            switch (data.type) {
                case 'startup':
                    // валидация
                    _.each(data.errors, function (errors, field) {
                        self.$form.trigger('form:setFieldErrors', [field, errors]);
                    });
                    showError({'message': gettext('Please, validate form data.')});
                    break;
                case 'transport':
                    // обработка
                    showError({'message': gettext('Connection lost.')});
                    break;
                case 'revoked':
                    // отмена
                    showError({'message': gettext('Operation canceled.')});
                    break;
                case 'failed':
                    // дропнуто в обработчике
                    showError({'message': gettext('Operation error.')});
                    break;
                default:
                    break;
            }
            /*console.log('fail', data);*/
        },

        getSettings: function () {
            var self = this;
            TaskWrapper(getLayoutData().urls.settings.receive)
                .create(showPreloader)
                .done(function (data) {
                    self.model.set(data);
                    self.model.flush();
                    showMessage({'message': gettext('Settings received successfully.')});
                })
                .fail(self.processErrors)
                .always(function () {
                    hidePreloader();
                });
        },

        saveSettings: function () {
            var self = this;
            TaskWrapper(getLayoutData().urls.settings.update)
                .create({
                    beforeStart: showPreloader,
                    data: self.model.toJSON()
                })
                .done(function (data) {
                    self.model.set(data);
                    showMessage({'message': gettext('Settings saved successfully.')});
                })
                .fail(self.processErrors)
                .always(function () {
                    hidePreloader();
                });
            return false;
        }

    }))();

});
