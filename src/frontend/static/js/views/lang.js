$(document).ready(function () {
    new (Backbone.View.extend({
        el: $('body'),

        events: {
            'click .js-set-lang': 'setLang'
        },

        initialize: function () {
            _.bindAll(this,
                'setLang');
        },

        setLang: function (event) {
            var $link = $(event.target).closest('a');
            $.ajax({
                url: '/i18n/',
                method: 'POST',
                data: {
                    next: '/',
                    language: $link.data('lang')
                }
            }).done(function () {
                window.location = '';
            });
            return false;
        }

    }))();
});
