$(document).ready(function () {
    new (Backbone.Epoxy.View.extend({
        el: $('.js-page-faq'),

        events: {
            'click .faq .title': 'toggle'
        },

        initialize: function () {
            var self = this;
            if (!self.$el.length) {
                return;
            }
            _.bindAll(self, 'toggle');
            self.$items = self.$el.find('.item');
        },

        toggle: function (event) {
            var self = this;
            var $item = $(event.target).closest('.item');
            self.$items.not($item).removeClass('active');
            $item.toggleClass('active');
        }

    }))();

});
