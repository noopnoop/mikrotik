# encoding: utf-8

from django.contrib import admin
from django_markdown.admin import MarkdownModelAdmin

from models import FAQItem


class FAQItemAdmin(MarkdownModelAdmin):
    list_display = (
        'question',
        'answer',
    )

    def get_queryset(self, request):
        queryset = super(FAQItemAdmin, self).get_queryset(request)
        queryset = queryset
        return queryset


admin.site.register(FAQItem, FAQItemAdmin)
