from pprint import pprint

from django.core.management.base import BaseCommand

from backend.models import Router
from frontend.client import RouterClient


class Command(BaseCommand):
    def handle(self, *args, **options):
        router = Router.objects.\
            filter(router_id='9JTL-BW89').\
            first()
        with RouterClient(router) as client:
            pprint([
                'client.put_settings',
                client.put_settings('testwifiname', 'testwifipass')
            ])
            pprint([
                'client.get_settings',
                client.get_settings()
            ])
