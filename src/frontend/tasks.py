# encoding: utf-8

from django.conf import settings
from celery import task

from .client import RouterClient


@task(name='get_settings')
def get_settings(client, *args, **kwargs):
    if settings.DEBUG_SSH_TASKS:
        return {
            'wifi_enabled': False,
            'wifi_ssid': '',
            'wifi_password': '',
            'wifi_channel': '',
        }
    with RouterClient(client.router) as client:
        return client.get_settings()


@task(name='put_settings')
def put_settings(client, form, *args, **kwargs):
    if settings.DEBUG_SSH_TASKS:
        return {
            'wifi_enabled': False,
            'wifi_ssid': '',
            'wifi_password': '',
            'wifi_channel': '',
        }
    with RouterClient(client.router) as client:
        return client.put_settings(**form)
