# encoding: utf-8

from os import chmod
from json import loads
from logging import getLogger
from weakref import ref
from tempfile import NamedTemporaryFile

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from paramiko import SSHClient, AutoAddPolicy

from backend.models import Template


logger = getLogger('router-client')


class EmptyTemplate(ValueError):
    pass


class RouterClient(object):
    u"""
    Адаптер для работы с роутером

    Реализован через контекстный менеджер:
        with RouterClient(request.client.router) as client:
            client.get_settings()
            client.put_settings(**form)

    """

    def __init__(self, router):
        self.ssh_client = SSHClient()
        self.ssh_client.set_missing_host_key_policy(AutoAddPolicy())
        self.router = ref(router)
        self.commands = []

    def __enter__(self):
        logger.info('connect...')
        logger.info(self.router().ip)

        with NamedTemporaryFile() as key_file:
            key_file.write((self.router().ssh_key or '').strip())
            key_file.flush()

            # chmod(key_file.name, 0x400)

            self.ssh_client.connect(self.router().ip,
                                    look_for_keys=False,
                                    allow_agent=False,
                                    username=settings.ROUTER_USERNAME,
                                    # password='',
                                    key_filename=key_file.name)
            self.commands = []

        logger.info('connected...')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('disconnect...')
        self.ssh_client.close()

    def _append(self, template_alias, **kwargs):
        ctx = kwargs.copy()
        ctx.update({
            'router_id': self.router().router_id,
            'serial_number': self.router().serial_number,
            'router_ip': self.router().ip,
            'comment': self.router().comment,
        })
        command = Template.objects.\
            get(alias=template_alias).\
            render(ctx)
        command = command.strip()
        # logger.info(u'%s: %s' % (template_alias, command))

        if not command:
            raise EmptyTemplate()

        self.commands.append(command)

    def _execute(self):
        commands = ';'.join(self.commands)
        logger.info('Sending: %s' % commands)
        __, stdout, __ = self.ssh_client.exec_command(commands)
        return stdout.readlines()

    def get_settings(self):
        u"""
        Забор настроек с роутера
        """

        logger.info('get settings...')

        self._append('router_get_setting_wifi_enabled')
        self._append('router_get_setting_wifi_ssid')
        self._append('router_get_setting_wifi_password')
        self._append('router_get_setting_wifi_channel')

        lines = self._execute()[-4:]
        logger.info('Received: "%s"' % ','.join(lines))
        lines.reverse()

        return {
            'wifi_enabled': loads(lines.pop()).get('wifi_enabled'),
            'wifi_ssid': loads(lines.pop()).get('wifi_ssid'),
            'wifi_password': loads(lines.pop()).get('wifi_password'),
            'wifi_channel': loads(lines.pop()).get('wifi_channel'),
        }

    def put_settings(self, wifi_enabled=False, wifi_ssid=None, wifi_password=None, wifi_channel=None):
        u"""
        Применение настроек на роутере
        """

        logger.info('put settings...')

        self._append('router_put_setting_wifi_enabled', wifi_enabled=wifi_enabled)

        if wifi_enabled:
            self._append('router_put_setting_wifi_ssid', wifi_ssid=wifi_ssid or '')
            self._append('router_put_setting_wifi_password', wifi_password=wifi_password or '')
            self._append('router_put_setting_wifi_channel', wifi_channel=wifi_channel or '')

        return self.get_settings()
