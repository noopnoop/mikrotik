# encoding: utf-8

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, reverse_lazy

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class MikrotikIndexDashboard(Dashboard):
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.Group(
            _(u'Бекэнд'),
            column=1,
            collapsible=True,
            children=[
                modules.ModelList(
                    _(u'Роутеры'),
                    column=1,
                    collapsible=False,
                    models=('backend.models.Router',
                            'backend.models.RouterCommandLog', ),
                ),
                modules.ModelList(
                    _(u'Серверы'),
                    column=1,
                    collapsible=False,
                    models=('backend.models.Server',
                            'backend.models.ServerCommandLog', ),
                ),
                modules.ModelList(
                    _(u'Служебное'),
                    column=1,
                    collapsible=False,
                    models=('backend.models.Template',
                            'backend.models.Tag', ),
                ),
            ]
        ))

        self.children.append((modules.Group(
            '',
            column=1,
            collapsible=True,
            children=[
                modules.ModelList(
                    _(u'Чекер'),
                    column=1,
                    collapsible=False,
                    models=('nameservers.models.*',),
                )
            ]
        )))

        self.children.append(modules.LinkList(
            _(u'Ссылки'),
            column=3,
            children=[
                {
                    'title': _(u'Вернуться на сайт'),
                    'url': '/',
                    'external': True,
                },
            ]
        ))

        self.children.append(modules.Group(
            _(u'Администрирование и поддержка'),
            column=1,
            collapsible=True,
            children=[
                modules.ModelList(
                    _(u'Администрирование'),
                    column=1,
                    collapsible=False,
                    models=('django.contrib.*', ),
                ),
                modules.ModelList(
                    _(u'Поддержка'),
                    column=1,
                    collapsible=False,
                    models=('frontend.models.*',
                            'tickets.models.*',),
                ),
            ]
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))


