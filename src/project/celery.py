import djcelery


djcelery.setup_loader()


CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERY_ALWAYS_EAGER = False
BROKER_BACKEND = "djkombu.transport.DatabaseTransport"
CELERY_REDIRECT_STDOUTS = False
