PIPELINE_CSS = {
    'assets': {
        'source_filenames': (
            'css/libs/bootstrap.css',
            'css/libs/flags.css',
            'css/preloader.css',
            'css/styles.css',
        ),
        'output_filename': 'css/assets.css',
    },
}

PIPELINE_JS = {
    'assets': {
        'source_filenames': (
            'js/libs/lodash.js',
            'js/libs/backbone.js',
            'js/libs/backbone.epoxy.js',
            'js/libs/parsley.js',
            'js/libs/bootstrap-growl.js',
        ),
        'output_filename': 'js/assets.js',
    },
    'jquery': {
        'source_filenames': (
            'js/libs/jquery-1.10.1.js',
        ),
        'output_filename': 'js/jquery.js',
    },
    'bootstrap': {
        'source_filenames': (
            'js/libs/bootstrap.js',
        ),
        'output_filename': 'js/bootstrap.js',
    },
    'application': {
        'source_filenames': (
            'js/tools/csrf.js',
            'js/tools/messaging.js',
            'js/tools/parsley-config.js',
            'js/tools/preloader.js',
            'js/tools/layout.js',
            'js/tools/task-wrapper.js',
            'js/views/form.js',
            'js/views/settings.js',
            'js/views/lang.js',
            'js/views/faq.js',
            'js/main.js',
        ),
        'output_filename': 'js/application.js',
    },
    'tickets': {
        'source_filenames': (
            'tickets/js/views//tickets/status.js',
        ),
        'output_filename': 'js/tickets.js',
    },
}
