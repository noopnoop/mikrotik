# encoding: utf-8

from os.path import join, basename, abspath

from django.utils.translation import ugettext_lazy as _


BASE_PATH = abspath(join(basename(__file__), '..'))

relative = lambda *args: join(BASE_PATH, *args)
relative_path = lambda *args: join(BASE_PATH, *args) + '/'


SITE_ID = 1
SECRET_KEY = '*4%x&amp;z@l+h)yavcd@f&amp;l7#+#b+nw+909ja*9a41hlz32ej7p8&amp;'
ALLOWED_HOSTS = ['*', ]

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Alex', 'alxgrmv@gmail.com'),
)
MANAGERS = ADMINS


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': relative('database.sqlite'),
#     }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'mikrotik',
        'USER': 'root',
        'PASSWORD': '654321',
        'HOST': '',
        'PORT': '',
    }
}


TIME_ZONE = 'Asia/Novosibirsk'

USE_I18N = True
USE_L10N = True
USE_TZ = True

# LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', _('English')),
    ('zh-cn', _('Chinese')),
)
LOCALE_PATHS = (
    relative_path('frontend/locale'),
)


MEDIA_ROOT = relative_path('media')
MEDIA_URL = '/media/'

STATIC_ROOT = relative_path('static')
STATIC_URL = '/static/'


STATICFILES_DIRS = ()
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'frontend.middleware.FrontendMiddleware',
    'frontend.middleware.HomeRedirectMiddleware',
    'frontend.middleware.AdminLocaleURLMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'pipeline.middleware.MinifyHTMLMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

ROOT_URLCONF = 'project.urls'
WSGI_APPLICATION = 'project.wsgi.application'

TEMPLATE_DIRS = ()

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'grappelli.dashboard',
    'grappelli',
    'django.contrib.admin',

    'raven.contrib.django.raven_compat',

    'rosetta',
    'statici18n',
    'modeltranslation',

    'pipeline',
    'crispy_forms',
    'django_markdown',

    'rest_framework',

    'south',
    'debug_toolbar',
    'django_extensions',

    'djcelery',
    'djkombu',

    'api',
    'backend',
    'nameservers',
    'frontend',
    'tickets',

)

TEMPLATE_CONTEXT_PROCESSORS = (
    "frontend.context_processors.client",
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'domains': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
    }
}


# базовый DNS-сервер, относительно которого выполняется сравнение
BASE_DNS = '8.8.8.8'

# количество дополнительных DNS-серверов по которым проверяется идентичность
DNS_SERVERS_COUNT = 5

# максимальное время в секундах для ожидания ответа DNS-сервера для резолва google.com.
# сервера которые отвечают дольше не будут взяты для проверки.
DNS_SERVER_TIMEOUT = 1.0

# максимальное время в секундах для ожидания ответа от одного DNS-сервера при резолве каждого проверяемого домена
DOMAIN_RESOLVE_TIMEOUT = 4.0


GRAPPELLI_ADMIN_TITLE = 'MIKROTIK DASHBOARD'


# при отладке обновления настроек все настройки будут сброшенными и никогда не будут уходить на роутер
DEBUG_SSH_TASKS = False
# CA_PATH = '/tmp/router/ca.crt'


CRISPY_TEMPLATE_PACK = 'bootstrap3'

STATICI18N_DOMAIN = 'djangojs'
STATICI18N_PACKAGES = ('frontend', )


MODELTRANSLATION_AUTO_POPULATE = 'default'
MODELTRANSLATION_FALLBACK_LANGUAGES = {
    'default': ('en', ),
    # 'ru': ('en', ),
}


EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'sdfsdfkjhsjkfh@gmail.com'
EMAIL_HOST_PASSWORD = '/home/alex/'


GRAPPELLI_INDEX_DASHBOARD = 'project.dashboard.MikrotikIndexDashboard'


ROUTER_USERNAME = 'server'


if DEBUG:
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'LOCATION': 'mikrotik'
        },
    }
else:
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': '127.0.0.1:11211',
        }
    }
ROSETTA_UWSGI_AUTO_RELOAD = True


REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
    )
}


STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'
try:
    from pipeline import *
except ImportError:
    pass


try:
    from .celery import *
except ImportError:
    pass


try:
    from local_settings import *
except ImportError:
    pass


if not DEBUG:
    RAVEN_CONFIG = {
        'dsn': 'http://419146997f59468ebb114cd9a82bb8be:9cb20eb779744aa8ba16817001b3d3a4@sentry.cathay.io/2',
    }
