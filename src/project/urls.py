from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^guru/', include(admin.site.urls)),
    url(r'^api/', include('api.urls')),

    url(r'^', include('backend.urls', namespace='backend')),
    url(r'^', include('frontend.urls')),
    url(r'^tickets/', include('tickets.urls')),

    url(r'^i18n/', include(patterns('',
        url('^$', 'django.views.i18n.set_language', name='set-lang'),
    ), namespace='i18n')),
    url(r'^rosetta/', include('rosetta.urls')),

    url('^markdown/', include('django_markdown.urls')),

)
