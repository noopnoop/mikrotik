# encoding: utf-8

import os.path

from fabric.api import *


env.root = os.path.join(os.path.dirname(__file__), '../')
env.venv = os.path.join(env.root, 'venv/bin/')
env.activate = os.path.join(env.venv, 'activate')
env.python = os.path.join(env.venv, 'python')
env.pip = os.path.join(env.venv, 'pip')


def local_update():
    u"""Обновление локальной копии репозитория"""

    local('git pull')

    with lcd(env.root):
        local('{} install -r ./src/requirements.txt'.format(env.pip))
        local('{} ./src/manage.py syncdb --noinput'.format(env.python))
        local('{} ./src/manage.py migrate --noinput'.format(env.python))
        local('{} ./src/manage.py collectstatic --noinput'.format(env.python))


def update_messages():
    u"""Сборка и компиляция файлов переводов"""

    with lcd(os.path.join(env.root, 'src', 'frontend')):
        local('rm -r static/jsi18n')
        local('{} ../manage.py makemessages --all -l en -l zh-cn'.format(env.python))
        local('{} ../manage.py makemessages --all -l en -l zh-cn -d djangojs'.format(env.python))
        local('{} ../manage.py compilemessages'.format(env.python))
        local('{} ../manage.py compilejsi18n -l en '.format(env.python))
        local('{} ../manage.py compilejsi18n -l zh-cn'.format(env.python))


def recheck():
    u"""Обновление доменов и команд роутеров"""

    with lcd(env.root):
        with lcd('tools'):
            local('./nameservers.py')
            local('./domains.py | ./poisoned.py | %s' % '{} ../src/manage.py update'.format(env.python))
