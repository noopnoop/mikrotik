# encoding: utf-8

from datetime import timedelta
from re import search

from django.contrib.auth import get_user_model
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from celery import task
from celery.task import periodic_task, PeriodicTask
from imbox import Imbox

from models import Ticket, TicketComment, OWNER_MANAGER, OWNER_USER


User = get_user_model()


def send_notify(subject, template, context):
    receivers = get_user_model().objects.\
        filter(is_superuser=True).\
        values_list('email', flat=True).\
        all()
    content = render_to_string(template, context)
    msg = EmailMultiAlternatives(subject,
                                 strip_tags(content),
                                 settings.EMAIL_HOST_USER,
                                 receivers)
    msg.attach_alternative(content, 'text/html')
    msg.send()


def get_emails():
    imbox = Imbox('imap.gmail.com',
                  settings.EMAIL_HOST_USER,
                  settings.EMAIL_HOST_PASSWORD,
                  ssl=True)

    messages = imbox.messages(folder='inbox', unread=True)

    for uid, message in messages:
        imbox.mark_seen(uid)
        yield message.sent_from[0].get('email'), \
              message.subject, \
              (message.body.get('plain') + [''])[0]


@task
def new_ticket(ticket_pk):
    ticket = Ticket.objects.get(pk=ticket_pk)
    send_notify(
        'Ticket #{}: {}'.format(ticket.pk,
                                ticket.title),
        'tickets/notifies/ticket.html',
        {
            'ticket': ticket,
        }
    )


@task
def new_ticket_comment(comment_pk):
    comment = TicketComment.objects.get(pk=comment_pk)
    if comment.owner != OWNER_USER:
        return
    send_notify(
        'Ticket #{}: {}'.format(comment.ticket.pk,
                                comment.ticket.title),
        'tickets/notifies/comment.html',
        {
            'comment': comment,
        }
    )


class StoreAnswers(PeriodicTask):
    ignore_result = True
    run_every = timedelta(minutes=10)
    name = 'tickets.tasks.store_answers'
    queue = 'tickets'
    options = {
        'queue': 'tickets',
    }

    def run(self, *args, **kwargs):
        for sent_from, title, message in get_emails():
            user = User.objects.filter(email=sent_from).first()
            if not user:
                print 'no user'
                continue

            result = search(r'#(\d+)', title)
            if not result:
                print 'no pk'
                continue

            try:
                pk = result.group(1)
            except ValueError:
                continue

            try:
                ticket = Ticket.objects.get(pk=pk)
            except Ticket.DoesNotExist:
                print 'no ticket'
                continue

            TicketComment.objects.create(
                ticket=ticket,
                owner=OWNER_MANAGER,
                comment=message
            )
