from django.conf.urls import patterns, include, url

import views


urlpatterns = patterns('',
    url(r'^', include(patterns('',
        url(r'^$', views.ListTicketView.as_view(), name='list'),
        url(r'^new/$', views.CreateTicketView.as_view(), name='create'),
        url(r'^status/(?P<pk>\d+)/$', views.ChangeTicketStatusView.as_view(), name='change-status'),
    ), namespace='tickets')),

    url(r'^', include(patterns('',
        url(r'^(?P<pk>\d+)/$', views.ListCommentView.as_view(), name='list'),
        url(r'^(?P<pk>\d+)/add/$', views.CreateCommentView.as_view(), name='add'),
    ), namespace='comments')),
)
