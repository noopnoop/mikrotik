$(document).ready(function () {
    new (Backbone.View.extend({
        el: $('.js-page-tickets'),

        events: {
            'click .js-statuses button': 'changeStatus'
        },

        initialize: function () {
            var self = this;

            if (!self.$el.length) {
                return;
            }

            self.$modal = self.$el.find('.js-change-status-modal');
            self.$form = self.$modal.find('form');

            _.bindAll(this, 'changeStatus');
        },

        changeStatus: function (event) {
            var self = this;
            var $button = $(event.target);
            var status = $button.data('status');

            self.$modal.modal('hide');
            showPreloader();

            self.$form.find('[name=status]').val(status);
            self.$form.submit();
        }

    }))();
});
