# encoding: utf-8

from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.utils.translation import ugettext_lazy as _
from celery import current_app

from backend.models import Router


STATUS_NEW, STATUS_IN_PROGRESS, STATUS_RESOLVED, STATUS_CLOSED = range(4)
statuses = {
    STATUS_NEW: {
        'title': _(u'New'),
        'css_class': 'danger',
        'is_closed': False,
    },
    STATUS_IN_PROGRESS: {
        'title': _(u'In Progress'),
        'css_class': 'info',
        'is_closed': False,
    },
    STATUS_RESOLVED: {
        'title': _(u'Resolved'),
        'css_class': 'success',
        'is_closed': True,
    },
    STATUS_CLOSED: {
        'title': _(u'Closed'),
        'css_class': 'default',
        'is_closed': True,
    },
}
status_choices = {
    index: item.get('title')
    for index, item in statuses.items()
}
status_classes = {
    index: item.get('css_class')
    for index, item in statuses.items()
}
closed_statuses = [
    index
    for index, item in statuses.items()
    if item.get('is_closed', False)
]

OWNER_SYSTEM, OWNER_MANAGER, OWNER_USER = range(3)
owners = {
    OWNER_SYSTEM: _(u'System'),
    OWNER_MANAGER: _(u'Manager'),
    OWNER_USER: _(u'User'),
}


class Ticket(models.Model):
    router = models.ForeignKey(Router,
                               verbose_name=_(u'Router'))
    user = models.ForeignKey(get_user_model(),
                             null=True, blank=True, default=None,
                             verbose_name=_(u'User'))

    status = models.SmallIntegerField(default=0,
                                      choices=status_choices.items(),
                                      verbose_name=_(u'Status'))

    title = models.CharField(max_length=1024,
                             verbose_name=_(u'Title'))
    description = models.TextField(verbose_name=_(u'Description'))

    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name=_(u'Created'))

    def get_absolute_url(self):
        return reverse('comments:list', kwargs={
            'pk': self.pk,
        })

    @property
    def status_name(self):
        return status_choices.get(self.status)

    @property
    def status_class(self):
        return status_classes.get(self.status)

    @property
    def is_closed(self):
        return self.status in closed_statuses

    @property
    def is_open(self):
        return not self.is_closed

    @classmethod
    def on_pre_save(cls, sender, instance, **kwargs):
        if not instance.pk:
            return
        ticket = Ticket.objects.get(pk=instance.pk)
        if ticket.status != instance.status:
            TicketComment.objects.create(
                ticket=ticket,
                owner=OWNER_SYSTEM,
                comment=_(u'Ticket status changed from **{}** to **{}**.').format(
                    ticket.status_name,
                    instance.status_name,
                )
            )

    @classmethod
    def on_post_save(cls, sender, instance, **kwargs):
        current_app.send_task('tickets.tasks.new_ticket',
                              args=(instance.pk, ),
                              queue='tickets')

    class Meta:
        verbose_name = _(u'Тикет')
        verbose_name_plural = _(u'Тикеты')


pre_save.connect(Ticket.on_pre_save, sender=Ticket)
post_save.connect(Ticket.on_post_save, sender=Ticket)


class TicketComment(models.Model):
    ticket = models.ForeignKey(Ticket,
                               related_name='comments',
                               verbose_name=_(u'Ticket'))

    owner = models.SmallIntegerField(default=OWNER_USER,
                                     choices=owners.items(),
                                     verbose_name=_(u'Comment owner'))

    comment = models.TextField(null=True, blank=True, default=None,
                               verbose_name=_(u'Comment'))

    created = models.DateTimeField(null=True, blank=True, default=None,
                                   auto_now_add=True,
                                   editable=False,
                                   verbose_name=_(u'Created'))

    def __unicode__(self):
        return (self.comment or '')[:20] + u'...'

    @property
    def owner_name(self):
        return owners.get(self.owner)

    @property
    def is_system_owner(self):
        return self.owner == OWNER_SYSTEM

    @property
    def is_manager_owner(self):
        return self.owner == OWNER_MANAGER

    @property
    def is_user_owner(self):
        return self.owner == OWNER_USER

    @classmethod
    def on_pre_save(cls, sender, instance, **kwargs):
        if instance.owner != OWNER_MANAGER:
            return
        ticket = instance.ticket
        if ticket.status == STATUS_NEW:
            ticket.status = STATUS_IN_PROGRESS
            ticket.save()

    @classmethod
    def on_post_save(cls, sender, instance, **kwargs):
        if instance.owner in (OWNER_USER, OWNER_MANAGER, ):
            current_app.send_task('tickets.tasks.new_ticket_comment',
                                  args=(instance.pk, ),
                                  queue='tickets')

    class Meta:
        verbose_name = _(u'Комментарий')
        verbose_name_plural = _(u'Комментарии')


pre_save.connect(TicketComment.on_pre_save, sender=TicketComment)
post_save.connect(TicketComment.on_post_save, sender=TicketComment)
