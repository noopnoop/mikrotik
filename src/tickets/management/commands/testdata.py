from random import randint, randrange

from django.db.transaction import atomic
from django.core.management.base import BaseCommand
from django.contrib.webdesign.lorem_ipsum import sentence, paragraph, paragraphs


class Command(BaseCommand):
    @atomic
    def handle(self, *args, **options):
        from backend.models import Router
        from tickets.models import (Ticket, TicketComment,
                                    statuses, owners)

        Ticket.objects.all().delete()

        for router in Router.objects.all():
            for _ in xrange(50):
                ticket = Ticket.objects.create(router=router,
                                               status=randint(0, len(statuses) - 1),
                                               title=sentence(),
                                               description=paragraph())
                for __ in xrange(0, randint(0, 50)):
                    TicketComment.objects.create(ticket=ticket,
                                                 owner=randint(0, len(owners) - 1),
                                                 comment=paragraph())
