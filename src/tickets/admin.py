from django.contrib import admin
from django_markdown.admin import MarkdownModelAdmin

from models import Ticket, TicketComment


class TicketCommentInline(admin.StackedInline):
    model = TicketComment


class TicketAdmin(MarkdownModelAdmin):
    inlines = (
        TicketCommentInline,
    )
    list_display = (
        'pk',
        'status',
        'router',
        'title',
        'description',
    )


admin.site.register(Ticket, TicketAdmin)
