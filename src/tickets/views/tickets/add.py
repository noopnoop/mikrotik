# encoding: utf-8

from django import forms
from django.views.generic import CreateView, UpdateView
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Div, Field, Submit
from django_markdown.widgets import MarkdownWidget

from tickets.models import Ticket, TicketComment
from ..base import TicketsMixin


class CreateTicketForm(forms.ModelForm):
    helper = FormHelper()
    helper.layout = Layout(
        Row(Div(Field('title'), css_class='col-xs-12')),
        Row(Div(Field('description'), css_class='col-xs-12')),
        Row(Div(Submit('submit', _(u'Submit new ticket'), css_class='btn-success pull-right'), css_class='col-xs-12')),
    )

    description = forms.CharField(widget=MarkdownWidget)

    class Meta:
        model = Ticket
        fields = ('title',
                  'description')


class CreateTicketView(TicketsMixin, CreateView):
    model = Ticket
    template_name = 'tickets/tickets/add.html'
    title = _(u'New ticket')
    form_class = CreateTicketForm

    def form_valid(self, form):
        form.instance.router = self.request.client.router
        return super(CreateTicketView, self).form_valid(form)
