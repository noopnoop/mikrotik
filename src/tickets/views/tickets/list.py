# encoding: utf-8

from django.db.models import Count
from django.views.generic import ListView
from django.utils.translation import ugettext_lazy as _

from tickets.models import Ticket, TicketComment
from ..base import TicketsMixin


class ListTicketView(TicketsMixin, ListView):
    model = Ticket
    template_name = 'tickets/tickets/list.html'
    title = _(u'Tickets')

    def get_queryset(self):
        queryset = super(ListTicketView, self).get_queryset()
        queryset = queryset.\
            filter(router=self.request.client.router).\
            annotate(comments_count=Count('comments')).\
            order_by('-pk')
        return queryset
