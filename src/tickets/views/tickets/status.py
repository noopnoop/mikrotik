# encoding: utf-8

from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseForbidden
from django import forms
from django.views.generic import UpdateView
from crispy_forms.helper import FormHelper

from tickets.models import Ticket, status_choices
from ..base import TicketsMixin


class ChangeStatusForm(forms.ModelForm):
    helper = FormHelper()

    status = forms.ChoiceField(choices=status_choices.items(),
                               widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        super(ChangeStatusForm, self).__init__(*args, **kwargs)
        status_field = self.fields.get('status')
        status_field.choices = filter(
            lambda item: item[0] > self.instance.status,
            status_field.choices
        )

    class Meta:
        model = Ticket
        fields = (
            'status',
        )


class ChangeTicketStatusView(TicketsMixin, UpdateView):
    model = Ticket
    form_class = ChangeStatusForm
    http_method_names = ('post', )

    def form_valid(self, form):
        if not self.is_mine_ticket(form.instance):
            return HttpResponseForbidden()
        return super(ChangeTicketStatusView, self).form_valid(form)

    def get_success_url(self):
        return reverse('comments:list', kwargs={
            'pk': self.kwargs.get('pk'),
        }) + '#bottom'
