# encoding: utf-8

from frontend.views.base import LayoutMixin, ClientRequiredMixin


class TicketsMixin(ClientRequiredMixin, LayoutMixin):
    def is_mine_ticket(self, ticket):
        return ticket.router == self.request.client.router

    def is_mine_comment(self, comment):
        return self.is_mine_ticket(comment.ticket)
