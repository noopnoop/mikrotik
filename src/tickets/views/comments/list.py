# encoding: utf-8

from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseForbidden
from django.views.generic import ListView
from django.utils.translation import ugettext_lazy as _

from tickets.models import Ticket, TicketComment, statuses
from ..base import TicketsMixin
from ..comments.add import CreateCommentForm
from ..tickets.status import ChangeStatusForm


class ListCommentView(TicketsMixin, ListView):
    model = TicketComment
    template_name = 'tickets/comments/list.html'
    title = _(u'Ticket')

    def get(self, request, *args, **kwargs):
        try:
            self.ticket = Ticket.objects.get(pk=self.kwargs.get('pk'))
        except Ticket.DoesNotExist:
            raise Http404()
        if not self.is_mine_ticket(self.ticket):
            return HttpResponseForbidden()
        return super(ListCommentView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super(ListCommentView, self).get_queryset()
        queryset = queryset.filter(ticket=self.ticket)
        return queryset

    def get_context_data(self, **kwargs):
        context_data = super(ListCommentView, self).get_context_data(**kwargs)

        add_comment_form = CreateCommentForm()
        add_comment_form.helper.form_action = reverse('comments:add', kwargs={
            'pk': self.kwargs.get('pk'),
        })

        change_status_form = ChangeStatusForm(instance=self.ticket)
        change_status_form.helper.form_action = reverse('tickets:change-status', kwargs={
            'pk': self.kwargs.get('pk'),
        })

        context_data.update({
            'ticket': self.ticket,
            'add_comment_form': add_comment_form,
            'change_status_form': change_status_form,
            'statuses': {
                key: value
                for key, value in statuses.iteritems()
                if key > self.ticket.status
            },
        })
        return context_data

    @property
    def title(self):
        return _(u'Ticket #{pk}').format(
            pk=self.kwargs.get('pk')
        )
