# encoding: utf-8

from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseForbidden
from django import forms
from django.views.generic import CreateView, UpdateView
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Div, Field, Submit
from django_markdown.widgets import MarkdownWidget

from tickets.models import Ticket, TicketComment, OWNER_USER, OWNER_MANAGER
from ..base import TicketsMixin


class CreateCommentForm(forms.ModelForm):
    helper = FormHelper()
    helper.layout = Layout(
        Row(Div(Field('comment'), css_class='col-xs-12')),
        Row(Div(Submit('submit', _(u'Submit'), css_class='btn-success pull-right'), css_class='col-xs-12')),
    )

    comment = forms.CharField(widget=MarkdownWidget)

    class Meta:
        model = TicketComment
        fields = (
            'comment',
        )


class CreateCommentView(TicketsMixin, CreateView):
    model = TicketComment
    template_name = 'tickets/comments/list.html'
    title = _(u'Add comment')
    form_class = CreateCommentForm

    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.instance.owner = OWNER_MANAGER
        else:
            form.instance.owner = OWNER_USER
        form.instance.ticket = Ticket.objects.get(pk=self.kwargs.get('pk'))
        if not self.is_mine_ticket(form.instance.ticket):
            return HttpResponseForbidden()
        return super(CreateCommentView, self).form_valid(form)

    def get_success_url(self):
        return reverse('comments:list', kwargs={
            'pk': self.kwargs.get('pk'),
        }) + '#bottom'
