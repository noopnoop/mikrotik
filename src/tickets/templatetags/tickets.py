# encoding: utf-8

from django import template
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from ..models import statuses


register = template.Library()


@register.inclusion_tag('tickets/tickets/statuses.html')
def ticket_statuses(ticket, add_class='left', *args, **kwargs):
    return {
        'ticket': ticket,
        'add_class': add_class,
        'statuses': {
            key: value
            for key, value in statuses.iteritems()
            if key > ticket.status
        },
    }
