# encoding: utf-8

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Domain(models.Model):
    ip = models.IPAddressField(verbose_name=_(u'IP'))
    name = models.CharField(max_length=255,
                            verbose_name=_(u'Название'))
    time = models.DateTimeField(auto_now_add=True,
                                null=True,
                                verbose_name=_(u'Время последнего обновления'))

    def __unicode__(self):
        return u'{} / {}'.format(self.name, self.ip)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.name = (self.name or '').lower()
        return super(Domain, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        unique_together = [
            ['ip', 'name', ],
        ]
        verbose_name = _(u'Домен')
        verbose_name_plural = _(u'Домены')


class DomainFilter(models.Model):
    name = models.CharField(max_length=255,
                            blank=True, null=True, default=None,
                            verbose_name=_(u'Название'))
    regexp = models.CharField(max_length=255,
                              unique=True,
                              verbose_name=_(u'Регулярное выражение'))

    def __unicode__(self):
        return u'{} / {}'.format(self.name, self.regexp)

    class Meta:
        verbose_name = _(u'Фильтр доменов')
        verbose_name_plural = _(u'Фильтры доменов')
