# encoding: utf-8

from sys import stdin
from logging import getLogger

from django.core.management.base import BaseCommand
from django.db.transaction import atomic

from nameservers.models import Domain


logger = getLogger('domains')


class Command(BaseCommand):
    help = u'Обновление состояний доменов и команд роутеров'

    @atomic
    def handle(self, *args, **options):
        logger.info(u'Удаление всех доменов...')
        Domain.objects.all().delete()

        logger.info(u'Добавление доменов...')
        for line in stdin:
            line = line.strip()
            if not line:
                continue
            domain, ip = line.split(',')
            Domain.objects.get_or_create(name=domain,
                                         ip=ip)

        logger.info(u'Обновление завершено')
