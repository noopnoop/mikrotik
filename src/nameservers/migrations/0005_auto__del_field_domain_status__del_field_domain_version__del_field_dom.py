# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Domain.status'
        db.delete_column(u'nameservers_domain', 'status')

        # Deleting field 'Domain.version'
        db.delete_column(u'nameservers_domain', 'version')

        # Deleting field 'Domain.added'
        db.delete_column(u'nameservers_domain', 'added')


    def backwards(self, orm):
        # Adding field 'Domain.status'
        db.add_column(u'nameservers_domain', 'status',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Domain.version'
        db.add_column(u'nameservers_domain', 'version',
                      self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Domain.added'
        db.add_column(u'nameservers_domain', 'added',
                      self.gf('django.db.models.fields.DateTimeField')(null=True),
                      keep_default=False)


    models = {
        u'nameservers.domain': {
            'Meta': {'unique_together': "[['ip', 'name']]", 'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'nameservers.domainfilter': {
            'Meta': {'object_name': 'DomainFilter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'regexp': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['nameservers']