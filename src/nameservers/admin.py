# encoding: utf-8

from django.contrib import admin

from models import Domain, DomainFilter


class DomainAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'ip',
        'time',
    )
    change_list_template = 'admin/domains-list.html'
    readonly_fields = (
        'time',
    )


class DomainFilterAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'regexp',
    )


admin.site.register(Domain, DomainAdmin)
admin.site.register(DomainFilter, DomainFilterAdmin)
