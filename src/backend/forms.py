# encoding: utf-8

from django import forms
from django.utils.translation import ugettext_lazy as _


class AddCommandsForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
    action = forms.CharField(widget=forms.HiddenInput)
    commands = forms.CharField(label=_(u'Команды'),
                               widget=forms.Textarea)
