# encoding: utf-8

from weakref import ref
from re import compile

from nameservers.models import Domain, DomainFilter


class DomainMap(object):
    u"""Итератор доменов с учетом лимита на один домен"""

    def __init__(self, router):
        queryset = Domain.objects
        self._router = ref(router)
        self._domains = queryset.order_by('time').all()
        self._filters = map(compile, DomainFilter.objects.values_list('regexp', flat=True))

    def __iter__(self):
        counts = {}
        for domain in self._domains:
            if self._is_filtered(domain.name):
                continue
            counts.setdefault(domain.name, 0)
            counts[domain.name] += 1
            if self._router().dns_short_list and counts[domain.name] > self._router().dns_short_list:
                continue
            yield domain

    def _is_filtered(self, domain):
        for domain_filter in self._filters:
            if domain_filter.search(domain):
                return True
        return False
