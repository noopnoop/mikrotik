# encoding: utf-8

from django.views.generic import DetailView
from django.template.response import HttpResponse

from base import ModelsMixin, BasicAuthMixin, UpdateLastSeenMixin


class StateView(ModelsMixin, BasicAuthMixin, UpdateLastSeenMixin, DetailView):
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        token = request.GET.get('token', '')
        if not token:
            return HttpResponse(status=400)

        log_item = self.log_model.objects.filter(token=token).first()
        if log_item:
            log_item.status = self.kwargs.get('result', 'err').lower()
            if log_item.status != 'ok':
                log_item.status = request.GET.get('msg', '')
                log_item.is_ok = False
            log_item.save()

        return HttpResponse()
