# encoding: utf-8

from django.utils.timezone import now
from django.utils.decorators import method_decorator

from backend.models import (Router, RouterCommandLog,
                            Server, ServerCommandLog)
from backend.auth import logged_in_or_basicauth


router_auth_func = lambda request, router_id, serial_number: Router.objects.\
    filter(router_id=router_id, serial_number=serial_number).\
    exists()


class UpdateLastSeenMixin(object):
    u"""Примесь для обновления last_seen"""
    def update_last_seen(self):
        if self.object and hasattr(self.object, 'last_seen'):
            self.object.last_seen = now()
            if hasattr(self.object, 'first_seen') and not self.object.first_seen:
                self.object.first_seen = now()
            self.object.save()


class BasicAuthMixin(object):
    @method_decorator(logged_in_or_basicauth(router_auth_func))
    def dispatch(self, request, *args, **kwargs):
        return super(BasicAuthMixin, self).dispatch(request, *args, **kwargs)


class ModelsMixin(object):
    models = {
        'router': Router,
        'server': Server,
    }
    log_models = {
        'router': RouterCommandLog,
        'server': ServerCommandLog,
    }

    def get_queryset(self):
        model = self.models[self.kwargs.get('type')]
        return model.objects

    @property
    def log_model(self):
        return self.log_models[self.kwargs.get('type')]
