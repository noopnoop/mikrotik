# encoding: utf-8

from django.db.transaction import atomic
from django.views.generic import DetailView
from django.template.response import HttpResponse
from django.utils.timezone import now

from backend.models import Router, Template
from base import ModelsMixin, BasicAuthMixin, UpdateLastSeenMixin


class ConfigView(ModelsMixin, BasicAuthMixin, UpdateLastSeenMixin, DetailView):
    template_name = 'backend/conf.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if not self.object:
            # такого объекта нет
            return HttpResponse(status=404)

        update_dns = isinstance(self.object, Router) and \
                     request.GET.get('dns', False)
        if update_dns:
            template = Template.objects.get(alias='dns_update_wrapper')
            domains_list = self.object.domains_list()
            self.object.add_commands(
                template.render({
                    'domains_list': domains_list,
                }),
                before=True,
                space='\n\n'
            )
            self.object.conf_update = self.object.conf_update.strip()
            self.object.dns_update_time = now()

        if not self.object.conf_update:
            # список команд пустой
            self.update_last_seen()
            return HttpResponse(status=200,
                                content='')

        with atomic():
            # создание записи лога
            log_item = self.log_model(**{
                self.object.__class__.__name__.lower(): self.object,
                'commands': self.object.conf_update,
            })
            log_item.save()

            # контекст
            context = self.get_context_data(object=self.object,
                                            log_item=log_item)

            # отрисовка ответа
            template = Template.objects.get(alias='config')
            response = HttpResponse(status=200,
                                    content=template.render(context))

            # сброс команд роутера
            self.object.conf_update = None
            self.object.save()

            #
            self.update_last_seen()

        return response
