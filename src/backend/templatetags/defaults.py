from django import template


register = template.Library()


@register.filter
def escape_slash(value):
    return value.replace('\\', '\\\\')
