# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Router', fields ['serial_number']
        db.delete_unique(u'backend_router', ['serial_number'])

        # Removing unique constraint on 'Router', fields ['ip']
        db.delete_unique(u'backend_router', ['ip'])

        # Adding model 'RouterCommandLog'
        db.create_table(u'backend_routercommandlog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('router', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.Router'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True)),
            ('token', self.gf('django.db.models.fields.TextField')()),
            ('commands', self.gf('django.db.models.fields.TextField')()),
            ('status', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'backend', ['RouterCommandLog'])


    def backwards(self, orm):
        # Deleting model 'RouterCommandLog'
        db.delete_table(u'backend_routercommandlog')

        # Adding unique constraint on 'Router', fields ['ip']
        db.create_unique(u'backend_router', ['ip'])

        # Adding unique constraint on 'Router', fields ['serial_number']
        db.create_unique(u'backend_router', ['serial_number'])


    models = {
        u'backend.router': {
            'Meta': {'object_name': 'Router'},
            'active_till': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'conf_update': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'dns_update_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'first_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_index': 'True'}),
            'last_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'router_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '9', 'primary_key': 'True', 'db_index': 'True'}),
            'serial_number': ('django.db.models.fields.CharField', [], {'max_length': '12', 'db_index': 'True'}),
            'ssh_key': ('django.db.models.fields.files.FileField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Tag']", 'symmetrical': 'False', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '32', 'null': 'True', 'blank': 'True'})
        },
        u'backend.routercommandlog': {
            'Meta': {'object_name': 'RouterCommandLog'},
            'commands': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'router': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.Router']"}),
            'status': ('django.db.models.fields.TextField', [], {}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.TextField', [], {})
        },
        u'backend.server': {
            'Meta': {'object_name': 'Server'},
            'conf_update': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'conf_user_template': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'unique': 'True', 'max_length': '15', 'db_index': 'True'}),
            'last_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'ssh_key': ('django.db.models.fields.files.FileField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {})
        },
        u'backend.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['backend']