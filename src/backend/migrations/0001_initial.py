# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tag'
        db.create_table(u'backend_tag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'backend', ['Tag'])

        # Adding model 'Server'
        db.create_table(u'backend_server', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
            ('ip', self.gf('django.db.models.fields.IPAddressField')(unique=True, max_length=15, db_index=True)),
            ('conf_update', self.gf('django.db.models.fields.TextField')(default='', null=True, blank=True)),
            ('ssh_key', self.gf('django.db.models.fields.files.FileField')(default=None, max_length=100, null=True, blank=True)),
            ('last_seen', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('conf_user_template', self.gf('django.db.models.fields.TextField')(default='', null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'backend', ['Server'])

        # Adding model 'Router'
        db.create_table(u'backend_router', (
            ('router_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=9, primary_key=True, db_index=True)),
            ('serial_number', self.gf('django.db.models.fields.CharField')(unique=True, max_length=12, db_index=True)),
            ('ip', self.gf('django.db.models.fields.IPAddressField')(unique=True, max_length=15, db_index=True)),
            ('added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True)),
            ('first_seen', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('last_seen', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('active_till', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('dns_update_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(default=None, max_length=75, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(default=None, max_length=32, null=True, blank=True)),
            ('ssh_key', self.gf('django.db.models.fields.files.FileField')(default=None, max_length=100, null=True, blank=True)),
            ('conf_update', self.gf('django.db.models.fields.TextField')(default='', null=True, blank=True)),
        ))
        db.send_create_signal(u'backend', ['Router'])

        # Adding M2M table for field tags on 'Router'
        m2m_table_name = db.shorten_name(u'backend_router_tags')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('router', models.ForeignKey(orm[u'backend.router'], null=False)),
            ('tag', models.ForeignKey(orm[u'backend.tag'], null=False))
        ))
        db.create_unique(m2m_table_name, ['router_id', 'tag_id'])


    def backwards(self, orm):
        # Deleting model 'Tag'
        db.delete_table(u'backend_tag')

        # Deleting model 'Server'
        db.delete_table(u'backend_server')

        # Deleting model 'Router'
        db.delete_table(u'backend_router')

        # Removing M2M table for field tags on 'Router'
        db.delete_table(db.shorten_name(u'backend_router_tags'))


    models = {
        u'backend.router': {
            'Meta': {'object_name': 'Router'},
            'active_till': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'conf_update': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'dns_update_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'first_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'unique': 'True', 'max_length': '15', 'db_index': 'True'}),
            'last_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'router_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '9', 'primary_key': 'True', 'db_index': 'True'}),
            'serial_number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '12', 'db_index': 'True'}),
            'ssh_key': ('django.db.models.fields.files.FileField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Tag']", 'symmetrical': 'False'}),
            'type': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '32', 'null': 'True', 'blank': 'True'})
        },
        u'backend.server': {
            'Meta': {'object_name': 'Server'},
            'conf_update': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'conf_user_template': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'unique': 'True', 'max_length': '15', 'db_index': 'True'}),
            'last_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'ssh_key': ('django.db.models.fields.files.FileField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {})
        },
        u'backend.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['backend']