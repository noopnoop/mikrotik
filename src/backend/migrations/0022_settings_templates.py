# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

DATA = [
    {
        'alias': 'router_get_setting_wifi_ssid',
        'name': u'Получение настроек. Название WIFI сети',
        'content': u""":put ("{\\"wifi_ssid\\":\\"" . [/ip firewall address-list get [find list=wifi] comment] . "\\"}")""",
        'description': u"""На выходе должен быть валидный JSON структуры {"wifi_ssid": "имя сети"}""",
    },
    {
        'alias': 'router_get_setting_wifi_password',
        'name': u'Получение настроек. Пароль WIFI сети',
        'content': u"",
        'description': u"""""",
    },
    {
        'alias': 'router_put_setting_wifi_ssid',
        'name': u'Сохранение настроек. Название WIFI сети',
        'content': u"/ip firewall address-list set [find list=wifi] comment={{ wifi_ssid }}",
        'description': u"""{wifi_ssid}""",
    },
    {
        'alias': 'router_put_setting_wifi_password',
        'name': u'Сохранение настроек. Пароль WIFI сети',
        'content': u"",
        'description': u"""""",
    },
]


class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        Template = orm['backend.Template']
        for item in DATA:
            Template.objects.get_or_create(**item)

    def backwards(self, orm):
        "Write your backwards methods here."
        Template = orm['backend.Template']
        for item in DATA:
            Template.objects.filter(alias=item.get('alias')).delete()

    models = {
        u'backend.router': {
            'Meta': {'unique_together': "[['router_id', 'serial_number'], ['router_id', 'ip']]", 'object_name': 'Router'},
            'active_till': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'conf_update': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'dns_short_list': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'dns_update_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'first_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'db_index': 'True'}),
            'last_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'router_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '9', 'primary_key': 'True', 'db_index': 'True'}),
            'serial_number': ('django.db.models.fields.CharField', [], {'max_length': '12', 'db_index': 'True'}),
            'ssh_key': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['backend.Tag']", 'symmetrical': 'False', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '32', 'null': 'True', 'blank': 'True'})
        },
        u'backend.routercommandlog': {
            'Meta': {'object_name': 'RouterCommandLog'},
            'commands': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_ok': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'router': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.Router']"}),
            'status': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'})
        },
        u'backend.server': {
            'Meta': {'object_name': 'Server'},
            'conf_update': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'conf_user_template': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'unique': 'True', 'max_length': '15', 'db_index': 'True'}),
            'last_seen': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'primary_key': 'True', 'db_index': 'True'}),
            'ssh_key': ('django.db.models.fields.files.FileField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'backend.servercommandlog': {
            'Meta': {'object_name': 'ServerCommandLog'},
            'commands': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_ok': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.Server']"}),
            'status': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'})
        },
        u'backend.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'backend.template': {
            'Meta': {'object_name': 'Template'},
            'alias': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64', 'db_index': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['backend']
    symmetrical = True
