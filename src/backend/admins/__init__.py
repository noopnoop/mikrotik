from logs.routers import RouterCommandLogAdmin
from lists.routers import RouterAdmin

from logs.servers import ServerCommandLogAdmin
from lists.servers import ServerAdmin
from templates import TemplateAdmin
