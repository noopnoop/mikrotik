# encoding: utf-8

from django.contrib import admin
from django.contrib.admin.templatetags.admin_list import _boolean_icon
from django.utils.translation import ugettext_lazy as _

from backend.models import Router, RouterCommandLog, ServerCommandLog
from base import BaseListMixin


class ServerAdmin(BaseListMixin, admin.ModelAdmin):
    log_model = ServerCommandLog
    log_filter_key = 'server__name__exact'

    list_display = [
        'name',
        'ip',
        'status_bool',
        'last_seen_bool',
        'logs',
    ]

    readonly_fields = [
        'last_seen',
    ]

    search_fields = [
        'name',
    ]

    list_filter = [
        'status',
    ]

    def status_bool(self, obj):
        return _boolean_icon(obj.status)
    status_bool.allow_tags = True
    status_bool.short_description = _(u'Статус')
    status_bool.admin_order_field = 'status'

    def last_seen_bool(self, obj):
        if obj.last_seen is None:
            return _boolean_icon(obj.last_seen)
        return obj.last_seen
    last_seen_bool.allow_tags = True
    last_seen_bool.short_description = _(u'Последний запрос')
    last_seen_bool.admin_order_field = 'last_seen'
