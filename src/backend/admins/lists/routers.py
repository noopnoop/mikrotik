# encoding: utf-8

from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.contrib.admin.templatetags.admin_list import _boolean_icon
from django.utils.translation import ugettext_lazy as _

from backend.models import Router, RouterCommandLog
from base import BaseListMixin


class RoutersChangeList(ChangeList):
    def get_queryset(self, request):
        has_errors_subquery = RouterCommandLog.objects\
            .filter(is_ok=False)\
            .distinct()\
            .values_list('router_id', flat=True)\
            .query

        queryset = super(RoutersChangeList, self).get_queryset(request)

        queryset = queryset.extra(select={
            'is_online': 'last_seen >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE)',
            'is_expired': '(active_till < UTC_TIMESTAMP()) OR active_till IS NULL',
            'has_errors': '{table_name}.router_id in ({sub_query})'.format(
                table_name=Router._meta.db_table,
                sub_query=str(has_errors_subquery),
            ),
        })

        return queryset


class RouterAdmin(BaseListMixin, admin.ModelAdmin):
    log_model = RouterCommandLog
    log_filter_key = 'router__router_id__exact'

    search_fields = [
        'router_id',
        'serial_number',
    ]

    readonly_fields = [
        'added',
        'first_seen',
        'last_seen',
        'dns_update_time',
    ]

    list_display = [
        'router_id',
        'serial_number',
        'ip',
        'is_online',
        'service_state',
        'logs',
        'is_good',
        'comment',
    ]
    list_display_links = list_display

    list_filter = [
        'tags',
    ]
    filter_horizontal = [
        'tags',
    ]

    fieldsets = (
        (None, {
            'fields': ('router_id', 'serial_number', 'ip', 'active_till', )
        }),
        (_(u'Статистика'), {
            'fields': ('added', 'first_seen', 'last_seen', 'dns_update_time', )
        }),
        (None, {
            'fields': ('email', 'type', 'tags', 'dns_short_list', 'comment', )
        }),
        (None, {
            'fields': ('conf_update', )
        }),
        (_(u'SSH ключи'), {
            'fields': ('ssh_key', 'ssh_public_key', )
        }),
        (_(u'SSL ключи'), {
            'fields': ('ssl_certificate', 'ssl_key', 'ssl_ca', )
        }),
    )

    def is_online(self, obj):
        return obj.is_online
    is_online.boolean = True
    is_online.short_description = _(u'Онлайн статус')
    is_online.admin_order_field = 'is_online'

    def service_state(self, obj):
        if obj.is_expired:
            return _boolean_icon(False)
        else:
            return obj.active_till
    service_state.allow_tags = True
    service_state.short_description = _(u'Статус сервиса')
    service_state.admin_order_field = 'active_till'

    def is_good(self, obj):
        return _boolean_icon(not obj.has_errors)
    is_good.allow_tags = True
    is_good.short_description = _(u'Без ошибок')
    is_good.admin_order_field = 'has_errors'

    def get_changelist(self, request, **kwargs):
        return RoutersChangeList
