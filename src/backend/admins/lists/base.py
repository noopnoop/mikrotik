# encoding: utf-8

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.shortcuts import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from backend.actions import commands_add


class BaseListMixin(object):
    log_model = None
    log_filter_key = None

    actions = [commands_add, ]

    def __init__(self, *args, **kwargs):
        if settings.DEBUG:
            self.list_display += [
                'config_update_link',
                'config_update_status_link',
            ]
        super(BaseListMixin, self).__init__(*args, **kwargs)

    def logs(self, obj):
        info = self.log_model._meta.app_label, self.log_model._meta.model_name
        return u'<a target="_blank" href="%s?%s=%s">%s</a>' % (
            reverse_lazy('admin:%s_%s_changelist' % info),
            self.log_filter_key,
            obj.pk,
            _(u'Перейти')
        )
    logs.allow_tags = True
    logs.short_description = _(u'Лог')

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(BaseListMixin, self).get_readonly_fields(request, obj)
        if (obj is not None) and (self.model._meta.pk.name not in readonly_fields):
            return [self.model._meta.pk.name, ] + readonly_fields
        return readonly_fields

    if settings.DEBUG:
        def config_update_link(self, obj):
            return u'<a target="_blank" href="%s">%s</a>' % (
                reverse_lazy('backend:config-update', kwargs={
                    'type': self.model.__name__.lower(),
                    'pk': obj.pk,
                }),
                _(u'Перейти')
            )
        config_update_link.allow_tags = True
        config_update_link.short_description = _(u'URL команд')

        def config_update_status_link(self, obj):
            return u'<a target="_blank" href="%s">%s</a>' % (
                reverse_lazy('backend:config-update-state', kwargs={
                    'type': self.model.__name__.lower(),
                    'pk': obj.pk,
                    'result': 'ok',
                }),
                _(u'Перейти')
            )
        config_update_status_link.allow_tags = True
        config_update_status_link.short_description = _(u'URL статуса')

        change_list_template = 'admin/entities-list.html'

        def get_urls(self):
            urls = super(BaseListMixin, self).get_urls()
            urls.insert(0, url(r'random/$', self.random_router_view))
            return urls

        def random_router_view(self, request):
            count = int(request.REQUEST.get('count', 1))
            for _ in xrange(count):
                item = self.model()
                if hasattr(item, 'random'):
                    item.random()
                item.save()

            return HttpResponseRedirect('../')
