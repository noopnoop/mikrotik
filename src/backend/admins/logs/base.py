# encoding: utf-8

from django.contrib import admin


class BaseCommandLogAdmin(admin.ModelAdmin):
    related = None

    list_display = [
        'token',
        'time',
        'status',
        'is_ok',
    ]

    def __init__(self, *args, **kwargs):
        self.list_filter = list(self.list_filter) + [self.related, ]
        self.list_display = [self.related, ] + self.list_display
        super(BaseCommandLogAdmin, self).__init__(*args, **kwargs)

    def get_queryset(self, request):
        queryset = super(BaseCommandLogAdmin, self).get_queryset(request)
        queryset.prefetch_related(self.related)
        return queryset
