# encoding: utf-8

from django.contrib import admin

from base import BaseCommandLogAdmin


class RouterCommandLogAdmin(BaseCommandLogAdmin, admin.ModelAdmin):
    related = 'router'
