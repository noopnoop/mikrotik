# encoding: utf-8

from django.contrib import admin

from base import BaseCommandLogAdmin


class ServerCommandLogAdmin(BaseCommandLogAdmin, admin.ModelAdmin):
    related = 'server'
