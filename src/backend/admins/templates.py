# encoding: utf-8

from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _


class TemplateAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'desc',
        'alias',
    )

    ordering = ['alias', ]

    def desc(self, obj):
        return mark_safe(u'<pre>%s</pre>' % obj.description)
    desc.short_description = _(u'Описание')

    def get_readonly_fields(self, request, obj=None):
        return (
            'description',
            'name',
            'alias',
        )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False
