# encoding: utf-8

from os import remove
from subprocess import Popen, PIPE

from django.conf import settings
from django.db.transaction import atomic
from celery import Task

from backend.models import Router


class CreateKeysTask(Task):
    name = 'create-keys'
    ignore_result = True

    command = 'true'
    if not settings.DEBUG:
        command = 'sudo -Hn -u easyrsa /home/easyrsa/build-key {router_id}'

    @atomic
    def run(self, router_id, *args, **kwargs):
        router = Router.objects.get(pk=router_id)

        process = Popen(self.command.format(router_id=router.router_id),
                        shell=True,
                        stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()

        if process.returncode:
            raise Exception()

        router.ssl_certificate = self.getfile('/tmp/router/{}.crt'.format(router.router_id),
                                              do_remove=True)
        router.ssl_key = self.getfile('/tmp/router/{}.key'.format(router.router_id),
                                      do_remove=True)
        router.ssl_ca = self.getfile(getattr(settings, 'CA_PATH', '/tmp/router/ca.crt'))
        router.save()

    def getfile(self, filename, do_remove=False):
        data = ''.join(open(filename, 'r').readlines())
        if do_remove:
            remove(filename)
        return data
