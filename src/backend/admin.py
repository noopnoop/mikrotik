# encoding: utf-8

from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from models import (Tag, Template,
                    Server, ServerCommandLog,
                    Router, RouterCommandLog)

import admins


admin.site.register(Tag)
admin.site.register(Template, admins.TemplateAdmin)
admin.site.register(Server, admins.ServerAdmin)
admin.site.register(ServerCommandLog, admins.ServerCommandLogAdmin)
admin.site.register(Router, admins.RouterAdmin)
admin.site.register(RouterCommandLog, admins.RouterCommandLogAdmin)
