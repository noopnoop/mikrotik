# encoding: utf-8

from django.db.transaction import atomic
from django.contrib import admin
from django.shortcuts import HttpResponseRedirect, render
from django.utils.translation import ugettext_lazy as _

from forms import AddCommandsForm


def commands_add(modeladmin, request, queryset):
    initial = {
        'action': commands_add.__name__,
    }
    form = None
    if 'apply' in request.POST:
        form = AddCommandsForm(initial=initial, data=request.POST)
        if form.is_valid():
            commands = form.cleaned_data['commands']
            with atomic():
                for item in queryset:
                    if not item.conf_update:
                        item.conf_update = commands
                    else:
                        item.conf_update += commands
                    item.save()
            return HttpResponseRedirect(request.get_full_path())
    if not form:
        initial.update({
            '_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        })
        form = AddCommandsForm(initial=initial)
    return render(
        request,
        'admin/add-commands.html',
        {
            'title': _(u'Добавление команд'),
            'name': modeladmin.model._meta.verbose_name_plural,
            'items': queryset,
            'form': form,
            'action': commands_add.__name__
        }
    )
commands_add.short_description = _(u'Добавить команды')
