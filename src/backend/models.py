# encoding: utf-8

from random import randint
from uuid import uuid1
from re import match, sub
from datetime import timedelta

from django.contrib.auth import get_user_model
from django.conf import settings
from django.db import models
from django.db.transaction import atomic
from django.db.models.signals import post_save, pre_delete
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.template import Context, Template as DjangoTemplate
from django.utils.timezone import now
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _
from netaddr import IPNetwork, IPRange
from celery import current_app

from nameservers.models import DomainFilter
from utils import DomainMap


def router_id_validator(value):
    if match(r'[\d\w]{4}-[\d\w]{4}', value):
        return
    raise ValidationError(_(u'Router ID должен иметь формат XXXX-XXXX!'))


def serial_number_validator(value):
    if match(r'[\d\w]{12}', value):
        return
    raise ValidationError(_(u'SerialN должен иметь формат XXXXXXXXXXXX!'))


remove_spaces = lambda text: '\n'.join(map(lambda line: line.rstrip(), (text or '').splitlines()))  # sub(r'\ |\t', '', text).strip() if text is not None else None


class CommandLogBase(models.Model):
    time = models.DateTimeField(auto_now_add=True,
                                null=True,
                                verbose_name=_(u'Время забора конфига'))
    token = models.CharField(max_length=32,
                             unique=True,
                             verbose_name=_(u'Переданный токен'))
    commands = models.TextField(verbose_name=_(u'Выполненные команды'))
    is_ok = models.BooleanField(editable=False, default=True,
                                verbose_name=_(u'Без ошибок'))
    status = models.TextField(null=True, blank=True,
                              verbose_name=_(u'Результат выполнения'))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.token:
            self.token = uuid1().hex
        return super(CommandLogBase, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        abstract = True


class Tag(models.Model):
    name = models.CharField(max_length=32,
                            verbose_name=_(u'Название'))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _(u'Тег')
        verbose_name_plural = _(u'Теги')


class Server(models.Model):
    name = models.CharField(unique=True, blank=False, db_index=True,
                            primary_key=True,
                            max_length=255,
                            verbose_name=_(u'Название сервера'))
    ip = models.IPAddressField(unique=True, blank=False, db_index=True,
                               verbose_name=_(u'IP'))
    conf_update = models.TextField(null=True, blank=True, default='',
                                   verbose_name=_(u'Ожидающие команды'))
    ssh_key = models.FileField(null=True, blank=True, default=None,
                               upload_to='ssh-keys/servers/',
                               verbose_name=_(u'Приватный ключ'))
    last_seen = models.DateTimeField(null=True,
                                     verbose_name=_(u'Последний запрос'))
    conf_user_template = models.TextField(null=True, blank=True, default='',
                                          verbose_name=_(u'Шаблон команды для создания нового пользователя'))
    status = models.BooleanField(default=False,
                                 verbose_name=_(u'Состояние'),
                                 choices=((False, _(u'Выключен')),
                                          (True, _(u'Включен'))))

    if settings.DEBUG:
        def random(self):
            name = get_random_string(length=10).upper()
            ip = '%d.%d.%d.%d' % (randint(0, 255), randint(0, 255), randint(0, 255), randint(0, 255))

            self.name, self.ip = name, ip

    def __unicode__(self):
        return u'%(name)s' % {
            'name': self.name,
        }

    class Meta:
        verbose_name = _(u'Сервер')
        verbose_name_plural = _(u'Серверы')


class Router(models.Model):
    router_id = models.CharField(max_length=9,
                                 unique=True, blank=False, db_index=True,
                                 primary_key=True,
                                 validators=[router_id_validator],
                                 verbose_name=_(u'RouterID'))
    serial_number = models.CharField(max_length=12,
                                     blank=False, db_index=True,
                                     validators=[serial_number_validator],
                                     verbose_name=_(u'SerialN'))
    ip = models.IPAddressField(blank=False, db_index=True,
                               verbose_name=_(u'IP'))

    added = models.DateTimeField(null=True, auto_now_add=True,
                                 verbose_name=_(u'Добавлен'))
    first_seen = models.DateTimeField(null=True,
                                      verbose_name=_(u'Первый запрос'))
    last_seen = models.DateTimeField(null=True,
                                     verbose_name=_(u'Последний запрос'))
    active_till = models.DateTimeField(null=True,
                                       verbose_name=_(u'Время прекращения сервиса'))
    dns_update_time = models.DateTimeField(null=True,
                                           verbose_name=_(u'Время последней выгрузки обновлений DNS для роутера'))

    email = models.EmailField(null=True, blank=True, default=None,
                              verbose_name=_(u'Адрес для связи'))
    type = models.CharField(max_length=32,
                            null=True, blank=True, default=None,
                            verbose_name=_(u'Тип устройства'))

    conf_update = models.TextField(null=True, blank=True, default='',
                                   verbose_name=_(u'Ожидающие команды'))

    dns_short_list = models.PositiveIntegerField(null=True, blank=True, default=0,
                                                 verbose_name=_(u'Максимальное число записей для домена на один домен'))

    tags = models.ManyToManyField(Tag,
                                  blank=True,
                                  verbose_name=_(u'Теги роутера'))

    added_by = models.ForeignKey(get_user_model(),
                                 null=True, default=None, blank=True,
                                 verbose_name=_(u'Добавлено'))

    comment = models.TextField(blank=True, null=True, default=None,
                               verbose_name=_(u'Комментарий'))

    ssh_key = models.TextField(null=True, blank=True, default=None,
                               verbose_name=u'SSH key')
    ssh_public_key = models.TextField(null=True, blank=True, default=None,
                                      verbose_name=u'SSH public key')

    ssl_certificate = models.TextField(null=True, blank=True, default=None,
                                       verbose_name=u'Certificate')
    ssl_key = models.TextField(null=True, blank=True, default=None,
                               verbose_name=u'Key')
    ssl_ca = models.TextField(null=True, blank=True, default=None,
                              verbose_name=u'CA')

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.ssh_key = remove_spaces(self.ssh_key)
        self.ssh_public_key = remove_spaces(self.ssh_public_key)
        self.ssl_certificate = remove_spaces(self.ssl_certificate)
        self.ssl_key = remove_spaces(self.ssl_key)
        self.ssl_ca = remove_spaces(self.ssl_ca)

        if self.router_id:
            self.router_id = self.router_id.upper()
        if self.serial_number:
            self.serial_number = self.serial_number.upper()
        if not self.active_till:
            self.active_till = now() + timedelta(days=365)
        if not self.ip:
            # routers_range = IPRange('10.0.0.0', '10.255.255.255')
            query = Router.objects.extra(select={'rawip': 'INET_ATON(ip)', },
                                         # where=['rawip >= %s', 'rawip <= %s', ],
                                         # params=[routers_range.first, routers_range.last, ],
                                         order_by=['-rawip', ])
            last_ip = query.values_list('ip', flat=True)[0]
            self.ip = str(IPNetwork(last_ip or '10.1.0.100').next().ip)
        return super(Router, self).save(force_insert, force_update, using, update_fields)

    def add_commands(self, commands, before=False, space='\n'):
        u"""Добавляет команды текущему роутеру"""
        if before:
            self.conf_update = (commands or '') + space + (self.conf_update or '')
        else:
            self.conf_update = (self.conf_update or '') + space + (commands or '')
        self.conf_update = self.conf_update.strip()

    def domains_list(self):
        u"""Список доменов для текущего роутера"""
        commands = []
        template = Template.objects.get(alias='dns_add_domain')
        for domain in DomainMap(self):
            commands.append(template.render({
                'ip': domain.ip,
                'name': domain.name,
            }))
        return u'\n'.join(commands)

    def __unicode__(self):
        return u'%(router_id)s' % {
            'router_id': self.router_id,
        }

    if settings.DEBUG:
        def random(self):
            router_id = get_random_string(length=8)
            router_id = '%s-%s' % (router_id[:4], router_id[4:])
            ip = '%d.%d.%d.%d' % (randint(0, 255), randint(0, 255), randint(0, 255), randint(0, 255))
            serial_number = get_random_string(length=12).upper()

            self.router_id, self.serial_number, self.ip = router_id, serial_number, ip

    @classmethod
    @atomic
    def handler_event(cls, domain_filter, event):
        u"""Обработчик шаблона при измении фильтров"""
        template = Template.objects.get(alias=event)
        commands = template.render({
            'regexp': domain_filter.regexp,
            'name': domain_filter.name,
        })
        for router in Router.objects.all():
            router.add_commands(commands)
            router.save()

    @classmethod
    def before_regexp_delete(cls, sender, instance, **kwargs):
        cls.handler_event(instance, 'regexp_remove')

    @classmethod
    def after_regexp_update(cls, sender, instance, **kwargs):
        cls.handler_event(instance, 'regexp_add')

    @classmethod
    @atomic
    def on_save(cls, sender, instance, created, **kwargs):
        if not created:
            return
        last_task = cache.get(instance.router_id, None)
        if last_task:
            return
        result = current_app.send_task('create-keys', kwargs={'router_id': instance.pk, })
        cache.set(instance.router_id, result.task_id)

    class Meta:
        verbose_name = _(u'Роутер')
        verbose_name_plural = _(u'Роутеры')

        unique_together = [
            ['router_id', 'serial_number', ],
            ['router_id', 'ip', ],
        ]

post_save.connect(Router.on_save, sender=Router)
post_save.connect(Router.after_regexp_update, sender=DomainFilter)
pre_delete.connect(Router.before_regexp_delete, sender=DomainFilter)


class RouterCommandLog(CommandLogBase):
    router = models.ForeignKey(Router,
                               verbose_name=_(u'Роутер'))

    class Meta:
        verbose_name = _(u'Лог команд роутера')
        verbose_name_plural = _(u'Логи команд роутеров')


class ServerCommandLog(CommandLogBase):
    server = models.ForeignKey(Server,
                               verbose_name=_(u'Сервер'))

    class Meta:
        verbose_name = _(u'Лог команд сервера')
        verbose_name_plural = _(u'Логи команд серверов')


class Template(models.Model):
    alias = models.CharField(max_length=64, db_index=True, unique=True,
                             verbose_name=_(u'Уникальный идентификатор шаблона'))
    name = models.CharField(max_length=255, verbose_name=_(u'Название'))
    content = models.TextField(verbose_name=_(u'Содержимое шаблона'))
    description = models.TextField(verbose_name=_(u'Описание'))

    def __unicode__(self):
        return self.name or self.alias

    @property
    def template(self):
        if not self._template:
            self._template = DjangoTemplate(self.content)
        return self._template
    _template = None

    def render(self, context):
        return self.template.render(Context(context))

    class Meta:
        verbose_name = _(u'Шаблон')
        verbose_name_plural = _(u'Шаблоны')
