from django.conf import settings
from django.conf.urls import patterns, include, url

from views import ConfigView, StateView


urlpatterns = patterns('',
    url(r'^(?P<type>router|server)/(?P<pk>[\d\w-]+)/confupdate$',
        ConfigView.as_view(), name='config-update'),
    url(r'^(?P<type>router|server)/(?P<pk>[\d\w-]+)/confupdate/(?P<result>ok|err)$',
        StateView.as_view(), name='config-update-state'),
)
